param (
    [Parameter()]
    [ValidateNotNull()]
    [object[]]$Machines = @(@{ Name = $env:COMPUTERNAME })
)

$cred = Get-DevAppsCredential

$d = Get-Date -Format "yyyy-MM--dd-hh-mm-ss"
Start-Transcript -Path "C:\git\DevOps\Logs\SetOctopusTentacleAccount-log-$d.txt"

$output_path = 'c:\devops\SetOctopusTentacleAccount\'
Remove-Item $output_path -Recurse -Force -ErrorAction SilentlyContinue

$ntrights = "C:\Program Files (x86)\Windows Resource Kits\Tools\ntrights.exe"

foreach ($machine in $Machines) {
        
    Write-Host 
    Write-Host "$($machine.Name)" -ForegroundColor Cyan
    Write-Host 

    $ntrightsArgs = @("+r"
        "SeServiceLogonRight",
        "-u",
        "devapps",
        "-m",
        "\\$($machine.Name).local.imodules.com")
     

    & $ntrights $ntrightsArgs     
    function Set-ServiceCredential {
        [CmdletBinding(SupportsShouldProcess = $true, ConfirmImpact = "High")]
        param(
            $serviceName,
            $computerName,
            $serviceCredential,
            $connectionCredential
        )
        # Get computer name if passed by property name.
        if ( $computerName.ComputerName ) {
            $computerName = $computerName.ComputerName
        }
        # Empty computer name or . is local computer.
        if ( (-not $computerName) -or $computerName -eq "." ) {
            $computerName = [Net.Dns]::GetHostName()
        }
        $wmiFilter = "Name='{0}' OR DisplayName='{0}'" -f $serviceName
        $params = @{
            "Namespace"    = "root\CIMV2"
            "Class"        = "Win32_Service"
            "ComputerName" = $computerName
            "Filter"       = $wmiFilter
            "ErrorAction"  = "Stop"
        }
        if ( $connectionCredential ) {
            # Specify connection credentials only when not connecting to the local computer.
            if ( $computerName -ne [Net.Dns]::GetHostName() ) {
                $params.Add("Credential", $connectionCredential)
            }
        }
        try {
            $service = Get-WmiObject @params
        }
        catch [System.Management.Automation.RuntimeException], [System.Runtime.InteropServices.COMException] {
            Write-Error "Unable to connect to '$computerName' due to the following error: $($_.Exception.Message)"
            return
        }
        if ( -not $service ) {
            Write-Error "Unable to find service named '$serviceName' on '$computerName'."
            return
        }

        # See https://msdn.microsoft.com/en-us/library/aa384901.aspx
        $returnValue = ($service.Change($null, $null, $null, $null, $null, $null, $serviceCredential.UserName, $serviceCredential.GetNetworkCredential().Password)).ReturnValue
        $errorMessage = "Error setting credentials for service '$serviceName' on '$computerName'"
        switch ( $returnValue ) {
            0 {
                Write-Verbose "Set credentials for service '$serviceName' on '$computerName'"
                Restart-Service -DisplayName $serviceName
            }
            1 {
                Write-Error "$errorMessage - Not Supported" 
            }
            2 {
                Write-Error "$errorMessage - Access Denied" 
            }
            3 {
                Write-Error "$errorMessage - Dependent Services Running" 
            }
            4 {
                Write-Error "$errorMessage - Invalid Service Control" 
            }
            5 {
                Write-Error "$errorMessage - Service Cannot Accept Control" 
            }
            6 {
                Write-Error "$errorMessage - Service Not Active" 
            }
            7 {
                Write-Error "$errorMessage - Service Request timeout" 
            }
            8 {
                Write-Error "$errorMessage - Unknown Failure" 
            }
            9 {
                Write-Error "$errorMessage - Path Not Found" 
            }
            10 {
                Write-Error "$errorMessage - Service Already Stopped" 
            }
            11 {
                Write-Error "$errorMessage - Service Database Locked" 
            }
            12 {
                Write-Error "$errorMessage - Service Dependency Deleted" 
            }
            13 {
                Write-Error "$errorMessage - Service Dependency Failure" 
            }
            14 {
                Write-Error "$errorMessage - Service Disabled" 
            }
            15 {
                Write-Error "$errorMessage - Service Logon Failed" 
            }
            16 {
                Write-Error "$errorMessage - Service Marked For Deletion" 
            }
            17 {
                Write-Error "$errorMessage - Service No Thread" 
            }
            18 {
                Write-Error "$errorMessage - Status Circular Dependency" 
            }
            19 {
                Write-Error "$errorMessage - Status Duplicate Name" 
            }
            20 {
                Write-Error "$errorMessage - Status Invalid Name" 
            }
            21 {
                Write-Error "$errorMessage - Status Invalid Parameter" 
            }
            22 {
                Write-Error "$errorMessage - Status Invalid Service Account" 
            }
            23 {
                Write-Error "$errorMessage - Status Service Exists" 
            }
            24 {
                Write-Error "$errorMessage - Service Already Paused" 
            }
        }
            
    }
    
    $SecurePassWord = ConvertTo-SecureString -AsPlainText "Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf" -Force
    $devappsCred = New-Object System.Management.Automation.PSCredential ("devapps@local.imodules.com", $SecurePassWord)

    $octopusServiceName = "$($machine.Name)_TentacleV2"
    Set-ServiceCredential -serviceCredential $devappsCred -serviceName $octopusServiceName
}

Stop-Transcript
