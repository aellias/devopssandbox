﻿<#PSScriptInfo
.VERSION 1.0.0.1
.GUID 92b1397f-a14d-4175-a4c0-780c61c008f2
.AUTHOR rellias/jjones
.COMPANYNAME iModules Software Inc
.COPYRIGHT (c) 2018 iModules Software Inc. All rights reserved.
.TAGS Profile
.EXTERNALMODULEDEPENDENCIES 
.LICENSEURI
.PROJECTURI
.ICONURI
.EXTERNALMODULEDEPENDENCIES
.REQUIREDSCRIPTS
.EXTERNALSCRIPTDEPENDENCIES
.RELEASENOTES
.DESCRIPTION 
Run the dsc for a buildserver/agent
#>

param (
    [Parameter()]
    [ValidateNotNull()]
    [object[]]$Machines = @(@{ Name = "juggernaut" })
)

Configuration BuildAgent
{
    param($NodeName)
    
    Import-Module Encompass.Dsc
    Import-DscResource -ModuleName Encompass.Dsc
    Import-DscResource -Module TeamCityAgentDSC

    [DSCLocalConfigurationManager()]
    Configuration LcmConfig
    {
        Node "localhost"
        {
            Settings
            {
                RefreshMode = "Push"
                ConfigurationMode = "ApplyOnly"
                RebootNodeIfNeeded = $true
                MaximumDownloadSizeMB = 4000
            }
        }
    } 
    
    Node $NodeName
    {
        cBuildEnvironmentChocolateyInstalls chocolateyInstalls {
        }

        #cTeamCityAgent TeamCityAgent 
        #{ 
        #    Ensure = "Present" 
        #    State = "Stopped"         
        #    AgentName = "$NodeName"             
        #    ServerHostname = "teamcitysand.local.imodules.com"
        #    ServerPort = 90;
        #    AgentHostname = "$NodeName";
        #    AgentPort = 9090;            
        #    AgentHomeDirectory = "C:\TeamCity\Agent";
        #    AgentWorkDirectory = "C:\TeamCity\Agent\Work";    
        #}

        #cOctopusTentacle octopusTentacle {
        #    TentacleNameSuffix = "2"
        #    ApiKey             = 'API-BD6I4ZJV0VPNLOO6TQTDLET8BE'
        #    OctopusServerUrl   = 'http://octopussand.local.imodules.com:90/'
        #    Environments       = 'BuildServer'
        #    Roles              = 'tc-buildagent'
        #    Thumbprint         = '528414DF10103A60ABBCB78B75336DAE4BFEA591'
        #    Ensure             = 'Present'
        #    State              = 'Started'
        #}

        
        $proget_npm_repo_url = "http://proget.local.imodules.com:25000/npm/npm/"

        cYarnRegistry YarnRegistry{}
    }
}
import-module Encompass.Dsc
$cred = Get-DevAppsCredential

$d = Get-Date -Format "yyyy-MM--dd-hh-mm-ss"
Start-Transcript -Path "C:\git\DevOps\Logs\BuildAgentDsc-log-$d.txt"

$output_path = 'c:\devops\BuildAgentDsc\'
Remove-Item $output_path -Recurse -Force -ErrorAction SilentlyContinue
foreach ($machine in $Machines) {
    Write-Host
    Write-Host "Started $($machine.Name)" -ForegroundColor Cyan
    Write-Host
    
    BuildAgent -outputpath $output_path -NodeName "$($machine.Name)"
    Start-DscConfiguration -Path $output_path -ComputerName $machine.Name -Wait -Force -Verbose -Credential $cred

    Write-Host
    Write-Host "Finished $($machine.Name)" -ForegroundColor Cyan
    Write-Host
}

Stop-Transcript
