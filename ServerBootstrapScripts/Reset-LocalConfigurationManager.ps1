#[CmdletBinding()]
#Param(
#    [Parameter()]
#    [string[]]
#    $ComputerName = @('localhost')
#)

param (
    [Parameter()]
    [ValidateNotNull()]
    [object[]]$Machines = @(@{ Name = $env:COMPUTERNAME })
)

[DscLocalConfigurationManager()]
Configuration ResetLCM {
    Param (
        [String[]]
        $NodeName
    )
    Node $NodeName {
        Settings {
            ActionAfterReboot              = 'ContinueConfiguration'
            AllowModuleOverwrite           = $false
            CertificateID                  = $null
            ConfigurationDownloadManagers  = @{} 
            ConfigurationID                = $null
            ConfigurationMode              = 'ApplyAndMonitor'
            ConfigurationModeFrequencyMins = 15
            DebugMode                      = @('NONE')
            MaximumDownloadSizeMB          = 4000
            RebootNodeIfNeeded             = $True
            RefreshFrequencyMins           = 30
            RefreshMode                    = 'PUSH'
            ReportManagers                 = @{}
            ResourceModuleManagers         = @{}
            SignatureValidations           = @{}
            StatusRetentionTimeInDays      = 10
        }
    }
}

$cred = Get-DevAppsCredential

$d = Get-Date -Format "yyyy-MM--dd-hh-mm-ss"
Start-Transcript -Path "C:\git\DevOps\Logs\ResetLcm-log-$d.txt"

$output_path = 'c:\devops\ResetLcm\'
Remove-Item $output_path -Recurse -Force -ErrorAction SilentlyContinue


Import-Module Encompass.Devops
$computers = Get-MachinesByEnv 'Development'# | where-object {$_.Status -like 'next'}

foreach ($machine in $computers) {
    Write-Host
    Write-Host "Started $($machine.Name)" -ForegroundColor Cyan
    Write-Host

    ResetLCM -outputpath $output_path -NodeName "$($machine.Name)"
    Set-DscLocalConfigurationManager -Path $output_path -ComputerName $machine.Name -Verbose -force -Credential $cred

    Write-Host
    Write-Host "Finished $($machine.Name)" -ForegroundColor Cyan
    Write-Host
}

Stop-Transcript
