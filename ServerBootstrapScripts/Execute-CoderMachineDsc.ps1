﻿<#PSScriptInfo
.VERSION 1.0.0.0
.GUID 75239cf9-6ad9-40eb-95b1-d6b3d0906ae7
.AUTHOR rellias/jjones
.COMPANYNAME iModules Software Inc
.COPYRIGHT (c) 2018 iModules Software Inc. All rights reserved.
.TAGS Profile
.EXTERNALMODULEDEPENDENCIES 
.LICENSEURI
.PROJECTURI
.ICONURI
.EXTERNALMODULEDEPENDENCIES
.REQUIREDSCRIPTS
.EXTERNALSCRIPTDEPENDENCIES
.RELEASENOTES
.DESCRIPTION 
Run the dsc for a developer machine
#> 

param (
    [Parameter()]
    [ValidateNotNull()]
    [object[]]$Machines = @(@{ Name = $env:COMPUTERNAME })
)

Configuration CoderMachine
{
    param($NodeName)
    
    Import-Module Encompass.Dsc
    Import-DscResource -ModuleName Encompass.Dsc
    Node localhost
    {
        
        
        cYarnRegistry YarnRegistry {
        }
        
        cPowershellModules PowershellModules{
            
        }

        cBuildEnvironmentChocolateyInstalls chocolateyInstalls {
            
        }

        cCoderEnvironmentVariables CoderEnvironmentVariables {

        }
    }
}

$cred = Get-DevAppsCredential

$d = Get-Date -Format "yyyy-MM--dd-hh-mm-ss"
Start-Transcript -Path "C:\git\DevOps\Logs\CoderMachineDsc-log-$d.txt"

$output_path = 'c:\devops\CoderMachineDsc\'
Remove-Item $output_path -Recurse -Force -ErrorAction SilentlyContinue

foreach ($machine in $Machines) {
    CoderMachine -outputpath $output_path  -NodeName $machine.Name  
    #Start-DscConfiguration -Path $output_path -ComputerName $machine.Name -Verbose -Wait -Force -Credential $cred
}

Stop-Transcript
