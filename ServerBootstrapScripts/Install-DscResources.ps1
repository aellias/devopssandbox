param (
    [Parameter()]
    [ValidateNotNull()]
    [object[]]$Machines = @(@{ Name = $env:COMPUTERNAME })
)

Import-Module Encompass.Devops

$cred = Get-DevAppsCredential

$d = Get-Date -Format "yyyy-MM--dd-hh-mm-ss"
Start-Transcript -Path "C:\git\DevOps\Logs\InstallDscResources-log-$d.txt"

$output_path = 'c:\devops\InstallDscResources\'
Remove-Item $output_path -Recurse -Force -ErrorAction SilentlyContinue

foreach ($machine in $Machines) {

    function Set-MaxEnvelopeSizeForChocolatey {
        Write-Host "Increasing max envelope size to 4GB" -ForegroundColor Green
        Set-WSManInstance -ValueSet @{MaxEnvelopeSizekb = "4000000"} -ResourceURI 'winrm/config' -ComputerName $machine.Name -Credential $cred 
    }

    Set-MaxEnvelopeSizeForChocolatey

    Invoke-Command -ComputerName $machine.Name -Credential $cred -ScriptBlock {
        Write-Host 
        Write-Host "$env:COMPUTERNAME" -ForegroundColor Cyan
        Write-Host 

        function Setup-Chocolatey {
            if ($env:ChocolateyInstall -eq $null) {
                Write-Host "Installing Chocolatey Package Manager for Windows" -ForegroundColor Green
                iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
            }

            Write-Host "Allowing global confirmation for Chocolatey Packages" -ForegroundColor Green
            choco feature enable --name=allowGlobalConfirmation

            Write-Host "Installing nuget CLI" -ForegroundColor Green
            cinst nuget.commandline

            $hasNuget = Find-PackageProvider -Name "Nuget" -ForceBootstrap
            if ($hasNuget -eq $null) {    
                Write-Host "Installing Nuget Package Provider" -ForegroundColor Green
                Install-PackageProvider Nuget -verbose -Force
            }

            Write-Host "Set PSGallery InstallationPolicy as Trusted" -ForegroundColor Green
            Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
        }

        function Install-DscResourceModules {
            
            Write-Host "Installing Required DscResources" -ForegroundColor Green
            $coreModules = $("PsDscResources", "xPSDesiredStateConfiguration", "cChoco", "xPendingReboot")
            foreach ($module in $coreModules) {
            
                Write-Host "Uninstalling $module" -ForegroundColor Green
                Uninstall-Module $module -ErrorAction SilentlyContinue -AllVersions -Force
                Write-Host "Installing $module"  -ForegroundColor Green
                Install-Module $module -AllowClobber
            }

            Write-Host "Installing OctopusDSC" -ForegroundColor Green
            Uninstall-Module "OctopusDSC" -ErrorAction SilentlyContinue -AllVersions -Force
            Install-Module "OctopusDSC" -AllowClobber -RequiredVersion 4.0.376
        
            #cPowerShellPackageManagement needs RequiredVersion, Repository
            $cPowerShellPackageManagement = "cPowerShellPackageManagement"
            Write-Host "Uninstalling $cPowerShellPackageManagement" -ForegroundColor Green
            Uninstall-Module $cPowerShellPackageManagement -ErrorAction SilentlyContinue -AllVersions -Force
            Write-Host "Installing $cPowerShellPackageManagement" -ForegroundColor Green
            Install-Module $cPowerShellPackageManagement -AllowClobber -RequiredVersion 1.0.1.0 -Repository "PSGallery"
        }

        function Setup-Proget {
            if (-not(Get-PSRepository 'InternalPowershellModules' -ErrorAction SilentlyContinue)) { 
                Write-Host "Registering InternalPowershellModules as script and module repository" -ForegroundColor Green

                $proget_module_feed_url = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellModules/"
                $proget_script_feed_url = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellScripts/"

                Register-PSRepository -Name 'InternalPowerShellModules' -SourceLocation $proget_module_feed_url -PublishLocation $proget_module_feed_url -InstallationPolicy Trusted -ScriptPublishLocation $proget_script_feed_url -ScriptSourceLocation $proget_script_feed_url
                Set-PSRepository  -Name InternalPowerShellModules -SourceLocation $proget_module_feed_url -PublishLocation $proget_module_feed_url -InstallationPolicy Trusted -ScriptPublishLocation $proget_script_feed_url -ScriptSourceLocation $proget_script_feed_url
            }
        }

        function Setup-EncompassPowershellModules {
                    
            Write-Host "Installing Required DscResources from InternalPowershellModules Repository" -ForegroundColor Green
            $encompassModules = $("Encompass.Dsc", "Encompass.Build", "Encompass.DevOps", "TeamCityAgentDSC")
            foreach ($module in $encompassModules) {
            
                Write-Host "Uninstalling $module" -ForegroundColor Green
                Uninstall-Module $module -ErrorAction SilentlyContinue -AllVersions -Force
                Write-Host "Installing $module" -ForegroundColor Green
                Install-Module $module -AllowClobber -Repository "InternalPowerShellModules"

            }
        }

        Setup-Chocolatey
        Install-DscResourceModules
        Setup-Proget
        Setup-EncompassPowershellModules
    }
    
    Write-Host 
    Write-Host "Done!" -ForegroundColor Cyan
    Write-Host 
}

Stop-Transcript
