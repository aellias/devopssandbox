

# The DSC configuration that will generate metaconfigurations
[DscLocalConfigurationManager()]
Configuration DscMetaConfigs
{
    param
    (
        [Parameter(Mandatory = $True)]
        [String]$RegistrationUrl,

        [Parameter(Mandatory = $True)]
        [String]$RegistrationKey,

        [Parameter(Mandatory = $True)]
        [String[]]$ComputerName,

        [Int]$RefreshFrequencyMins = 30,

        [Int]$ConfigurationModeFrequencyMins = 15,

        [String]$ConfigurationMode = 'ApplyAndMonitor',

        [String]$NodeConfigurationName,

        [Boolean]$RebootNodeIfNeeded = $False,

        [String]$ActionAfterReboot = 'ContinueConfiguration',

        [Boolean]$AllowModuleOverwrite = $False,

        [Boolean]$ReportOnly
    )

    if (!$NodeConfigurationName -or $NodeConfigurationName -eq '') {
        $ConfigurationNames = $null
    }
    else {
        $ConfigurationNames = @($NodeConfigurationName)
    }

    if ($ReportOnly) {
        $RefreshMode = 'PUSH'
    }
    else {
        $RefreshMode = 'PULL'
    }

    Node $ComputerName
    {
        Settings {
            RefreshFrequencyMins           = $RefreshFrequencyMins
            RefreshMode                    = $RefreshMode
            ConfigurationMode              = $ConfigurationMode
            AllowModuleOverwrite           = $AllowModuleOverwrite
            RebootNodeIfNeeded             = $RebootNodeIfNeeded
            ActionAfterReboot              = $ActionAfterReboot
            ConfigurationModeFrequencyMins = $ConfigurationModeFrequencyMins
            DebugMode                      = 'All'
        }

        if (!$ReportOnly) {
            ConfigurationRepositoryWeb AzureAutomationStateConfiguration {
                ServerUrl          = $RegistrationUrl
                RegistrationKey    = $RegistrationKey
                ConfigurationNames = $ConfigurationNames
            }

            ResourceRepositoryWeb AzureAutomationStateConfiguration {
                ServerUrl       = $RegistrationUrl
                RegistrationKey = $RegistrationKey
            }
        }

        ReportServerWeb AzureAutomationStateConfiguration {
            ServerUrl       = $RegistrationUrl
            RegistrationKey = $RegistrationKey
        }
    }
}


Import-Module Encompass.Devops

# Create the metaconfigurations
# TODO: edit the ComputerName and NodeConfigurationName below as needed for your use case
 #@( "MYSTIQUE","STORM","HAVOK","PSYLOCKE","JUGGERNAUT","BISHOP","SABRETOOTH","ROGUE","CYCLOPS","BEAST","COLOSSUS");
$Params = @{
    RegistrationUrl                = 'https://scus-agentservice-prod-1.azure-automation.net/accounts/d7b4b01f-185d-45a0-836e-9169b25e0335';
    RegistrationKey                = 'JTMVbqgunz1bPwQ5lD/FkB7Wz8Pjff/XM0VDqIesZIM9+tCIc15bx6kr/Hij30VTe+dzCTB/YaC1fT4Bc5QZYA==';
    ComputerName                   = @( "MYSTIQUE","STORM","HAVOK","PSYLOCKE","JUGGERNAUT","BISHOP","SABRETOOTH","ROGUE","CYCLOPS","BEAST","COLOSSUS");
    RefreshFrequencyMins           = 240;
    ConfigurationModeFrequencyMins = 240;
    NodeConfigurationName          = "CoderMachineConfig.localhost"
    RebootNodeIfNeeded             = $True;
    AllowModuleOverwrite           = $True;
    ConfigurationMode              = 'ApplyAndAutocorrect';
    ActionAfterReboot              = 'ContinueConfiguration';
    ReportOnly                     = $False; # Set to $True to have machines only report to AA DSC but not pull from it

}

$root_dir = "C:\git\DevOps"
$metaconfig_root_dir = "$root_dir\DscMetaConfigs"
if (Test-Path -Path $metaconfig_root_dir) {
    Remove-Item -Path $metaconfig_root_dir -Recurse -Force -ErrorAction SilentlyContinue
}
Push-Location $root_dir
DscMetaConfigs @Params
Set-DscLocalConfigurationManager -Path $metaconfig_root_dir
Pop-Location
