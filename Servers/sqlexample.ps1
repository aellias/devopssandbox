function Get-Sites($ServerName, $Sids, $DatabaseName) {
    Write-Host 'Starting task Get-Sites'
    $query = @"
    SELECT 
		su.url+'/?sid='+LTRIM(STR(si.site_info_id))+'&clearcache=site' as siteurl
	FROM 
		site_info si join site_urls su on si.site_info_id = su.site_info_id and su.is_primary_url = 1 and su.is_disabled = 0 
	where 
        si.site_info_id in ($Sids)
"@

    #$ServerName = 'devdbmaster'
    #$DatabaseName = 'imod_master'
    $connectionString = "Server=$ServerName;Database=$DatabaseName;User Id=websa;Password=bigsecret8526;"
    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($query, $connection)
    $connection.Open()
 
    $reader = $command.ExecuteReader()
 
    $results = @()
    while ($reader.Read()) {
        $row = @{}
        for ($i = 0; $i -lt $reader.FieldCount; $i++) {
            $row[$reader.GetName($i)] = $reader.GetValue($i)
        }
        $results += new-object psobject -property $row
    }
    $connection.Close();
 
    $results
    Write-Host $results
}

Get-Sites -ServerName 'devdbmaster' -DatabaseName 'imod_master' -Sids "-10001,-10301,-10302"