﻿function Smart-Search()
{   
    $TestSearch = "*Harvard*"
    Set-Location "C:\devops\testbin"

    $itemSum = Get-ChildItem -Recurse -ErrorAction SilentlyContinue
        Foreach($itemInst in $itemSum)
            {
                $itemName = $itemInst.name
                $itemFullName = $itemInst.fullname

                if(($itemName -clike $TestSearch) -eq $true)
                    {                    
                        write-host $itemName, $itemFullName

                        $newFileName = $itemFullName.Replace("Harvard","old") 

                        Rename-Item -Path $itemFullName -NewName $newFileName                        

                    }
            }
}

Smart-Search