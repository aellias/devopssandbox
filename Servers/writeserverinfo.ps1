﻿ 
function WriteConfigJson($config, $config_file_path) {
$config.Prod.Machines | Sort-Object -property role, Name | ConvertTo-Json | Out-File $config_file_path
}

$config_filepath = "C:\git\DevOps\powershellmodules\Encompass.Devops\Config\environment_machine_configs\Prod.Machines.json"
 
$config = Get-Content $config_filepath -Raw | ConvertFrom-Json
 
$machines = $config.Prod.Machines | Sort-Object -property Tenant, Name
 
$current_tenant = ""
foreach ($machine in $machines) {
if ($machine.Tenant -ne $current_tenant) {
 
write-host
write-host "$($machine.Tenant)" -ForegroundColor CYAN
write-host
 
$current_tenant = $machine.Tenant
 
}
write-host "$($machine.Name) roles: $($machine.Role)"
}
 
$config.Prod.Machines = $machines

WriteConfigJson $config "C:\git\DevOps\powershellmodules\Encompass.DSC\Qa.Machines.json"