﻿$disks = get-WmiObject win32_logicaldisk
write-host $disks
foreach ($disk in $disks) {
        $diskId = $disk.DeviceID
        $freeSpace = $disk.FreeSpace
        $size = $disk.Size

        if ( {$freeSpace / 1GB} -le 100) {
            $driveFolders = Get-ChildItem -Path "$diskId\"
            $agents = $driveFolders -match 'buildagent'
            
            "{0:N2} MB" -f ((Get-ChildItem C:\users\ -Recurse | Measure-Object -Property Length -Sum -ErrorAction Stop).Sum / 1MB)

        }

    }