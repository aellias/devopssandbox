import-module encompass.build -DisableNameChecking -Version 1.2.0.3	
#$machines = "bldsrv2", "bldsrv4", "bldsrv6", "bldsrv11", "bldsrv12", "bldsrv13", "bldsrv14", "bldsrv15", "bldsrv16", "bldsrv18", "bldsrv19"
$machines = "bldsrv2", "bldsrv4", "bldsrv6", "bldsrv11", "bldsrv12", "bldsrv13", "bldsrv14", "bldsrv15", "bldsrv16", "bldsrv18", "bldsrv19"

$source = "C:\git\DevOps\powershellmodules\Encompass.Build"
$destination_root = ""
    
foreach ($machine in $machines) {

    $destination_root = "\\$machine\c$\Program Files\WindowsPowerShell\Modules\encompass.build\1.2.0.3"
    $null = mkdir $destination_root -ErrorAction Continue

    $path = [System.IO.Path];

    Get-ChildItem $source -Recurse |  foreach {
            
        $destination = Join-Path -Path $destination_root -ChildPath $_.FullName.Substring($source.length)

        $destination_dir = [System.IO.Path]::GetDirectoryName("$destination")
        if ((Test-Path $destination_dir) -eq $False) {
            CreateDirectory -directoryName $destination_dir      
        }
            
        Copy-Item -Force -Path $_.FullName -Destination $destination 
    }

    $old_module_root = "\\$machine\c$\Program Files\WindowsPowerShell\Modules\encompass.build\1.2.0.2"
    if(test-path $old_module_root) {
        Remove-Item $old_module_root -Recurse -Force -ErrorAction SilentlyContinue
    }
}

