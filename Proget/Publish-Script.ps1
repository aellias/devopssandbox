param($Path)    

$Splatting = @{
    Path        = "$Path"
    Version     = '1.0.0' # http://semver.org ;)
    # GUID will randomly auto-generate for new scripts
    # Only specify GUID if you're going to overwrite existing properties on an updated script version
    Author      = ''
    Description = ''
    # CompanyName
    # Copyright
    Tags        = @('super', 'awesome', 'radical')
    # RequiredModules = 'Pester','AzureRM'  # or any module name published on the PS Gallery
    # ExternalModuleDependencies = 'qwerty' # any module name not published on the PS Gallery
    # RequiredScripts = 'asdf'              # any script published on the PS Gallery
    # ExternalScriptDependencies = 'jkl'    # you get the idea
    # ProjectURI = 'https://YourGitHubProject'
    # LicenseURI = 'https://YourGitHubProject/LICENSE'
    # IconURI
    # ReleaseNotes   # http://info.sapien.com/index.php/scripting/scripting-modules/get-release-notes
    Force       = $true
}
#Update-ScriptFileInfo @Splatting

Publish-Script -Path $Path -Repository "InternalPowerShellModules" -NuGetApiKey "1PAgtQMrXNEq7Z35_0xe" -Verbose   