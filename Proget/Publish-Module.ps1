param($name, $path, $updateManifestVersion = $true)    

function Get-PublicFunctions {
    param (
        $modulePath
    )
   
    if(Test-Path "$modulePath\public"){
        $fileNameList = new-object System.Collections.ArrayList    
        $files = Get-ChildItem -Path "$modulePath\public" -Recurse -file -include *.ps1

        foreach($fileName in $files) 
        {
            $file = Split-Path $fileName -leaf
            [void]$fileNameList.Add( $file.Replace('.ps1', ''));
        }
    }

    return $fileNameList
}

function Set-ModuleFileListInPsm {
    param (
        $path
    )
    $new_files = [System.Text.StringBuilder]::new()
    $files = Get-ChildItem -Path $path -Recurse -file -include *.ps1

    foreach($fileName in $files) 
    {
        $file_to_add = $fileName.FullName.Replace("$path\", "")
        [void]$new_files.Append(". `$PSScriptRoot\$file_to_add`n")
    }
    Set-Content -Path "$path\$name.psm1" -Value $new_files
}

function CommitAndPushToBitBucket {
    param(
        $path,
        $module_manifest_file
    )
    & git stage --all
    & git pull
    & git commit -m "Incremented module version in $module_manifest_file"   
    & git push
}

$paramsValid = $true
if([string]::IsNullOrEmpty($name)){
        Write-Host 'Module Name not supplied' -ForegroundColor Red
        $paramsValid = $false
}

if([string]::IsNullOrEmpty($path)){
        Write-Host 'Path not supplied' -ForegroundColor Red
        $paramsValid = $false
}

if ($paramsValid -eq $true) {
    if ($updateManifestVersion -eq $true) {   

        $module_manifest_file = "$path\$name.psd1"
        $publicFunctions = Get-PublicFunctions $path
        
        Set-ModuleFileListInPsm -Path $path
        Write-Host 'updated psm1' -ForegroundColor Green

        Set-ModuleFunctions -Name $module_manifest_file -FunctionsToExport $publicFunctions
        Write-Host 'set module functions in psd1' -ForegroundColor Green

        Update-MetaData -Path $module_manifest_file -Increment 'Revision'
        Write-Host 'updated version' -ForegroundColor Green

        CommitAndPushToBitBucket -Path $path -module_manifest_file $module_manifest_file
        
        #need to push to bitbucket before proget publish
        Publish-Module -Path $path -Repository "InternalPowerShellModules" -NuGetApiKey "1PAgtQMrXNEq7Z35_0xe" -Verbose        
    }
}



