﻿$modifier = 'US'
$appinstances = get-childitem "C:\devops\testapps\importtool.prod.$modifier.*.config"



if ($appinstances -ne $null) {
    foreach ($app in $appinstances) {
        $name = $app.basename
        write-output "=====================================name = $name================================================"
        $replace = $name -replace ".Prod.$modifier.", "_"
        if (($name.ToCharArray() -eq '.').count -eq 3) {
            $instance_dir = "$apps_folder\$replace"
        } elseif (($name.ToCharArray() -eq '.').count -gt 3) {
            $name_replace = $replace.Replace(".","_")
            $instance_dir = "$apps_folder\$name_replace"
        }
        Transform-AndCopyProdAppInstance -AppName $app_name -Transformer $webtransformer -WorkingDir $working_dir -InstanceDir $instance_dir -InstanceName $name -modifier $modifier
    }
}

<#
$url = "ImportTool.Prod.US.ProdSqlSite22.config"
($url.ToCharArray() -eq '.').count


if ($appinstances -ne $null) {
    foreach ($app in $appinstances) {
        $name = $app.basename
        write-host "=====================================name = $name================================================" -ForegroundColor Cyan
        $replace = $name -replace ".Prod.$modifier.", "_"
        if (($name.ToCharArray() -eq '.').count -eq 3) {
            $instance_dir = "$apps_folder\$replace"
            write-host "=====================================instance dir = $instance_dir================================================" -ForegroundColor Green
        } elseif (($name.ToCharArray() -eq '.').count -gt 3) {
            $name_replace = $replace1.Replace(".","_")
            $instance_dir = "$apps_folder\$name_replace"
            write-host "=====================================instance dir = $instance_dir================================================" -ForegroundColor Green
        }
    }
}


#>