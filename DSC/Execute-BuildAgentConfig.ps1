$ConfigData = @{
    AllNodes    = @(
        @{
            NodeName = 'bldsrv1'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv2'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv3'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv4'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv5'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv6'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv7'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv8'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv9'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv10'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv11'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv12'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv13'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv14'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv15'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv16'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv17'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv18'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv19'
            Role     = 'WebServer'
        },
        @{
            NodeName = 'bldsrv20'
            Role     = 'WebServer'
        }
    )
}

Start-AzureRmAutomationDscCompilationJob -ResourceGroupName 'DevOpsAuto' -AutomationAccountName 'DevOps-Automation' -ConfigurationName 'BuildAgent' -ConfigurationData $ConfigData