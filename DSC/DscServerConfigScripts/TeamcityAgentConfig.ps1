
Configuration TeamcityAgentConfig
{    
    Import-Module Encompass.Dsc
    Import-DscResource -ModuleName Encompass.Dsc
    
    #$MachineName = $env:COMPUTERNAME
    $MachineName = bldsrv1 

    Node bldsrv1
    {   
        cBuildEnvironmentChocolateyInstalls chocolateyInstalls {
        }
        
        cYarnRegistry YarnRegistry {
        }
        
        cPowershellModules PowershellModules{

        }
    }

}
