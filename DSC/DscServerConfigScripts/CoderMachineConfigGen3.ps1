
Configuration CoderMachineConfigGen3
{    
    Import-Module Encompass.Dsc
    Import-DscResource -ModuleName Encompass.Dsc

    Node localhost
    {   
        cCoderEnvironmentVariables coderEnvironmentVariables {
        }
           
        cBuildEnvironmentChocolateyInstalls chocolateyInstalls {
        } 
        
        cYarnRegistry YarnRegistry {
        }
        
        cPowershellModules PowershellModules{            
        }
        
        cPowershellProfile PowershellProfile {            
        }
    }

}
