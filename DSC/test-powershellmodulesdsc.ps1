﻿
Configuration PsModules
    {
    Script InstallPowershellModules {        

        TestScript = {

            $needsUpdate = $False
            $modules_list = @("Encompass.Build", "Encompass.Devops", "Encompass.Dsc", "psake")

            foreach ($module in $modules_list) {  

                $localModule = Get-Module -ListAvailable -Name "$module"
                Write-Debug "Local $($localModule.Name) has version $($localModule.Version )"
                Write-Verbose "Local $($localModule.Name) has version $($localModule.Version )"

                $galleryModule = Find-Module -Name "$module" -Repository 'InternalPowerShellModules' -ErrorAction Stop
                Write-Debug "Proget $($galleryModule.Name) has version $($galleryModule.Version )"
                Write-Verbose "Proget $($galleryModule.Name) has version $($galleryModule.Version )"

                if ($galleryModule.Version -gt $localModule.Version) {
                    Write-Debug "module: $module needs updating from proget"
                    Write-Verbose "module: $module needs updating from proget"
                    $needsUpdate = $True     
                }
            }

            return $needsUpdate
        }
            
        SetScript  = {
            
            if (-not(Get-PSRepository 'InternalPowershellModules' -ErrorAction SilentlyContinue)) {
 
                $proget_module_feed_url = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellModules/"
                $proget_script_feed_url = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellScripts/"
             
                Register-PSRepository -Name 'InternalPowerShellModules' -SourceLocation $proget_module_feed_url -PublishLocation $proget_module_feed_url -InstallationPolicy Trusted -ScriptPublishLocation $proget_script_feed_url -ScriptSourceLocation $proget_script_feed_url
                Set-PSRepository -Name 'InternalPowerShellModules' -SourceLocation $proget_module_feed_url -PublishLocation $proget_module_feed_url -InstallationPolicy Trusted -ScriptPublishLocation $proget_script_feed_url -ScriptSourceLocation $proget_script_feed_url
            }

            $modules_list = @("Encompass.Build", "Encompass.Devops", "Encompass.Dsc", "psake")
            foreach ($module in $modules_list) {       
                       
                $localModule = Get-Module -ListAvailable -Name "$module"
                if($localModule){        
                    Write-Debug "Local $($localModule.Name) has version $($localModule.Version )"                
                    Write-Verbose "Local $($localModule.Name) has version $($localModule.Version )"
                } else {
                    Install-Module -Name $module -Repository 'InternalPowerShellModules'
                }

                $galleryModule = Find-Module -Name "$module" -Repository 'InternalPowerShellModules' -ErrorAction Stop
                Write-Debug "Proget $($galleryModule.Name) has version $($galleryModule.Version )"

                if ($galleryModule.Version -gt $localModule.Version) {
                    Write-Debug "Updating module: $module from proget"
                    Write-Verbose "Updating module: $module from proget"
                    Uninstall-Module $module -ErrorAction Stop -AllVersions -Force
                    Install-Module -Name $module -Repository "InternalPowerShellModules" -Verbose        
                }
            }

        }
        
        GetScript  = {
             
            $localModule = Get-Module -ListAvailable -Name "Encompass.Dsc"
            return @{Result = $localModule.Version}
        }
    }
}
PsModules
        