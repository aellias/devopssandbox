﻿$7zip_path = 'C:\BuildServer\\BuildTools\7-Zip\7z.exe'
Set-Alias 7zip $7zip_path

$zippedFile = 'c:\devops\AzureDscResources'
$moduleDir = 'c:\Program Files\WindowsPowerShell\Modules'
7zip a -mx=1 "$zippedFile\Encompass.Dsc.zip" "C:\git\DevOps\powershellmodules\Encompass.Dsc\*"

7zip a -mx=1 "$zippedFile\xPendingReboot.zip" "$moduleDir\xPendingReboot\*"

7zip a -mx=1 "$zippedFile\cPowerShellPackageManagement.zip" "$moduleDir\cPowerShellPackageManagement\*"
7zip a -mx=1 "$zippedFile\Encompass.Devops.zip" "C:\git\DevOps\powershellmodules\Encompass.Devops\*"
7zip a -mx=1 "$zippedFile\xWebAdministration.zip" "$moduleDir\xWebAdministration\*"
7zip a -mx=1 "$zippedFile\cChoco.zip" "$moduleDir\cChoco\*"
7zip a -mx=1 "$zippedFile\OctopusDSC.zip" "$moduleDir\OctopusDSC\*"
7zip a -mx=1 "$zippedFile\TeamCityAgentDSC.zip" "$moduleDir\TeamCityAgentDSC\*"
7zip a -mx=1 "$zippedFile\xPSDesiredStateConfiguration.zip" "$moduleDir\xPSDesiredStateConfiguration\*"
7zip a -mx=1 "$zippedFile\PSDscResources.zip" "$moduleDir\PSDscResources\*"   