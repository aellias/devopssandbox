# The DSC configuration that will generate metaconfigurations for new azure dsc nodes
[DscLocalConfigurationManager()]
Configuration DscMetaConfigs
{
     param
     (
         [Parameter(Mandatory=$True)]
         [String]$RegistrationUrl,

         [Parameter(Mandatory=$True)]
         [String]$RegistrationKey,

         [Parameter(Mandatory=$True)]
         [String[]]$ComputerName,

         [Int]$RefreshFrequencyMins = 30,

         [Int]$ConfigurationModeFrequencyMins = 15,

         [String]$ConfigurationMode = 'ApplyAndMonitor',

         [String]$NodeConfigurationName,

         [Boolean]$RebootNodeIfNeeded= $False,

         [String]$ActionAfterReboot = 'ContinueConfiguration',

         [Boolean]$AllowModuleOverwrite = $False,

         [Boolean]$ReportOnly
     )

     if(!$NodeConfigurationName -or $NodeConfigurationName -eq '')
     {
         $ConfigurationNames = $null
     }
     else
     {
         $ConfigurationNames = @($NodeConfigurationName)
     }

     if($ReportOnly)
     {
         $RefreshMode = 'PUSH'
     }
     else
     {
         $RefreshMode = 'PULL'
     }

     Node $ComputerName
     {
         Settings
         {
             RefreshFrequencyMins           = $RefreshFrequencyMins
             RefreshMode                    = $RefreshMode
             ConfigurationMode              = $ConfigurationMode
             AllowModuleOverwrite           = $AllowModuleOverwrite
             RebootNodeIfNeeded             = $RebootNodeIfNeeded
             ActionAfterReboot              = $ActionAfterReboot
             ConfigurationModeFrequencyMins = $ConfigurationModeFrequencyMins
         }

         if(!$ReportOnly)
         {
         ConfigurationRepositoryWeb AzureAutomationStateConfiguration
             {
                 ServerUrl          = $RegistrationUrl
                 RegistrationKey    = $RegistrationKey
                 ConfigurationNames = $ConfigurationNames
             }

             ResourceRepositoryWeb AzureAutomationStateConfiguration
             {
             ServerUrl       = $RegistrationUrl
             RegistrationKey = $RegistrationKey
             }
         }

         ReportServerWeb AzureAutomationStateConfiguration
         {
             ServerUrl       = $RegistrationUrl
             RegistrationKey = $RegistrationKey
         }
     }
}

 # Create the metaconfigurations
 # NOTE: DSC Node Configuration names are case sensitive in the portal.
 # TODO: edit the below as needed for your use case
$Params = @{
    RegistrationUrl                 = 'https://scus-agentservice-prod-1.azure-automation.net/accounts/d7b4b01f-185d-45a0-836e-9169b25e0335';
    RegistrationKey                 = 'JTMVbqgunz1bPwQ5lD/FkB7Wz8Pjff/XM0VDqIesZIM9+tCIc15bx6kr/Hij30VTe+dzCTB/YaC1fT4Bc5QZYA==';
    ComputerName                    = @('qafs01');
     NodeConfigurationName = '';
     RefreshFrequencyMins = 30;
     ConfigurationModeFrequencyMins = 15;
     RebootNodeIfNeeded = $False;
     AllowModuleOverwrite = $False;
     ConfigurationMode = 'ApplyAndMonitor';
     ActionAfterReboot = 'ContinueConfiguration';
     ReportOnly = $False;  # Set to $True to have machines only report to AA DSC but not pull from it
}

# Use PowerShell splatting to pass parameters to the DSC configuration being invoked
# For more info about splatting, run: Get-Help -Name about_Splatting

$secpasswd = ConvertTo-SecureString 'Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf' -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ("devapps", $secpasswd)

DscMetaConfigs @Params
Set-DscLocalConfigurationManager -Path ./DscMetaConfigs -Credential $cred
#ComputerName                    = @('bldsrv1', 'bldsrv2', 'bldsrv3', 'bldsrv4', 'bldsrv5', 'bldsrv6', 'bldsrv7', 'bldsrv8', 'bldsrv9', 'bldsrv10', 'bldsrv11', 'bldsrv12', 'bldsrv13', 'bldsrv14', 'bldsrv15', 'bldsrv16', 'bldsrv17', 'bldsrv18', 'bldsrv19', 'bldsrv20');
