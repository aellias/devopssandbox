﻿$originalPath = 'C:\git\DevOps\powershellmodules\Encompass.Dsc'
$name = 'Encompass.Dsc'

# pathInModuleDir is the path where the symbolic link will be created which points to your repo
$pathInModuleDir = "C:\Program Files\WindowsPowerShell\Modules\$name"

New-Item -ItemType SymbolicLink -Path $pathInModuleDir -Target $originalPath