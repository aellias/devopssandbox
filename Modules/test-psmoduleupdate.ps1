﻿if (-not(Get-PSRepository 'InternalPowershellModules' -ErrorAction SilentlyContinue)) {
 
    $proget_module_feed_url = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellModules/"
    $proget_script_feed_url = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellScripts/"
             
    Register-PSRepository -Name 'InternalPowerShellModules' -SourceLocation $proget_module_feed_url -PublishLocation $proget_module_feed_url -InstallationPolicy Trusted -ScriptPublishLocation $proget_script_feed_url -ScriptSourceLocation $proget_script_feed_url
    Set-PSRepository -Name 'InternalPowerShellModules' -SourceLocation $proget_module_feed_url -PublishLocation $proget_module_feed_url -InstallationPolicy Trusted -ScriptPublishLocation $proget_script_feed_url -ScriptSourceLocation $proget_script_feed_url
}

$modules_list = @("Encompass.Build", "Encompass.Devops", "Encompass.Dsc", "Encompass.OctopusDeploy")
foreach ($module in $modules_list) {
                       
    $localModule = Get-Module -ListAvailable -Name "$module"
    if($localModule){        
        Write-output "Local $($localModule.Name) has version $($localModule.Version )"                
        Write-Verbose "Local $($localModule.Name) has version $($localModule.Version )"
    } else {
        Install-Module -Name $module -Repository 'InternalPowerShellModules'
    }

    $galleryModule = Find-Module -Name "$module" -Repository 'InternalPowerShellModules' -ErrorAction Stop
    #enc.build returns 2 values, get highest value
    if($galleryModule.Length -gt 1) { 
        $highest = $galleryModule.length - 1        
        $galleryModule = $gallerymodule[$highest]        
    }
    Write-output "Proget $($galleryModule.Name) has version $($galleryModule.Version )"

    if ($galleryModule.Version -gt $localModule.Version) {
        Write-Debug "Updating module: $module from proget"
        Write-Verbose "Updating module: $module from proget"
        Uninstall-Module $module -ErrorAction Stop -AllVersions -Force -Verbose
        Install-Module -Name $module -Repository "InternalPowerShellModules" -RequiredVersion $galleryModule.Version -Verbose
    }
}