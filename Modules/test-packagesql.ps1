﻿remove-module encompass.build
import-module encompass.build -DisableNameChecking -Version 1.2.0.3	

$build_working_dir = "C:\mercurial\encompass_kanban"
$packages_sql_dir = "c:\DevOps\sqlpkgs"
$revBaseStart = "c47a83fe44e7"


Package-Sql -build_working_dir $build_working_dir -packages_sql_dir $packages_sql_dir -branchName 'Release-61.1.0' -revBaseStart $revBaseStart | out-file c:\git\DevOps\Logs\pkgsqllogs\hglogprod.txt