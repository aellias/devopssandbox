﻿$prod_machines = "bldsrv11", "bldsrv12", "bldsrv13", "bldsrv14", "bldsrv15", "bldsrv16", "bldsrv18"

foreach ($machine in $prod_machines) {   
    $secpasswd = ConvertTo-SecureString 'Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf' -AsPlainText -Force
    $cred = New-Object System.Management.Automation.PSCredential ("devapps", $secpasswd)
    $SourcePath =  "C:\Program Files\WindowsPowerShell\Modules\encompass.build"
    $DestPath = "\\$machine\c$\Program Files\WindowsPowerShell\Modules"

    if(test-path "$DestPath\encompass.build" ) {
        remove-item "$DestPath\encompass.build" -recurse -Force
        write-host "removing build module on $machine"
    }
    Copy-Item -Recurse -Path $SourcePath -destination $DestPath 

    #invoke-command -ComputerName $machine -ScriptBlock { ipconfig } -Credential $cred
    
}