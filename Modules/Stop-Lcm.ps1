﻿$dscProcessID = Get-WmiObject msft_providers | 
    Where-Object { $_.provider -like 'dsccore' } | 
        Select-Object -ExpandProperty HostProcessIdentifier 


Get-Process -Id $dscProcessID | Stop-Process -Verbose

,
        @{
            NodeName     = "bldsrv2.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv3.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv4.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv5.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv6.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv7.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv8.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv9.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv10.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv11.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv12.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv13.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv14.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv15.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv16.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv17.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv18.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv19.local.imodules.com"
        },
        @{
            NodeName     = "bldsrv20.local.imodules.com"
        }