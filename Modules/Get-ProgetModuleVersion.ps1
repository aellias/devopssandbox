﻿function Get-ModuleVersion {
    $script:module_version 
    $proget_url = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellModules/Packages()?$format=json&$filter=Id eq 'Encompass.Build'&$select=Version"
     
    try {
        $wc = New-Object -TypeName System.Net.WebClient
        $uploadresult = $wc.UploadFile($proget_url, $zipfile)
    }
    catch [System.Net.WebException] {
        Write-Log -message $_.Exception.ToString()
        Write-Host -message "##teamcity[buildStatus status='FAILURE' text='Inside catch - A package failed to upload to the OctopusDeploy server']"
    }

    $apiKey = "SomeKey"
$resource = "http://proget.local.imodules.com:25000/nuget/InternalPowerShellModules/Packages()?$format=json&$filter=Id eq 'Encompass.Build'&$select=Version"

Invoke-RestMethod -Method Get -Uri $resource -Header @{ "X-ApiKey" = $apiKey }
}

Get-ModuleVersion