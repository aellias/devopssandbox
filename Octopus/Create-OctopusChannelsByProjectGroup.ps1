﻿import-module encompass.octopusdeploy

#$projects = 'EmailReactApps', 'EncompassReactApps', 'ScoreboardsReactApps'
#$octopus_steps_feature = , "DeployEncompass_QAWeb05_06"
$octopus_steps_feature = "Renew Redis Cache", "Encompass Offline Apps", "Encompass Database", "Encompass Email Services", "Encompass Webservices", "Encompass Database", "DeployEncompass_QAWeb05_06"


#$projectgroup = 'encompassMain'
$projectgroup = 'encompassDeployment'
$repository = Get-OctopusRepository


$projects = Get-OctopusProjectsByProjectGroupName -repository $repository -projectGroupName $projectgroup

foreach($proj in $projects) {
write-host $proj.name
    foreach($step in $octopus_steps_feature){        
        if($step -match $proj.name){
            write-host $step -ForegroundColor Magenta
            Create-OctopusProjectChannels -repository $repository -projectId $proj.Id
        }
    }   
}