function Exec {
    [CmdletBinding()]
    param(
        [Parameter(Position = 0, Mandatory = 1)][scriptblock]$cmd,
        [Parameter(Position = 1, Mandatory = 0)][string]$errorMessage = ($msgs.error_bad_command -f $cmd)
    )
    & $cmd
    if ($lastexitcode -ne 0) {
        throw ("Exec: " + $errorMessage)
    }
}

function Transform-ProdAppConfig {
    param(        
        [string]$AppName,
        [string]$Transformer,
        [string]$WorkingDir,
        [string]$modifier
    )
    
    $baseline_config = "$WorkingDir\$AppName.baseline.config"
    $prev_transformed_config = "$WorkingDir\$AppName.exe.config"
    $transformation = "$WorkingDir\$AppName.Prod.$modifier.config"
    $target = "$WorkingDir\$AppName.exe.config"

	#if running off staging/qa branch for testing, use prev transformed baseline
    if(!(test-path $baseline_config -ErrorAction SilentlyContinue)) {
        $baseline_config = $prev_transformed_baseline
    }

    #apply prod environment config
    exec {
        &$Transformer $baseline_config $transformation $target
    }

    Write-Host "Apply transform $transformation to $baseline_config and output $target"    
}