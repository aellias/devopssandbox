function Exec {
    [CmdletBinding()]
    param(
        [Parameter(Position = 0, Mandatory = 1)][scriptblock]$cmd,
        [Parameter(Position = 1, Mandatory = 0)][string]$errorMessage = ($msgs.error_bad_command -f $cmd)
    )
    & $cmd
    if ($lastexitcode -ne 0) {
        throw ("Exec: " + $errorMessage)
    }
}

function Transform-AndCopyProdAppInstance {    
    param(        
        [string]$AppName,
        [string]$AppInstallDir,
        [string]$Transformer,
        [string]$WorkingDir,
        [string]$InstanceDir,
        [string]$InstanceName,
        [string]$modifier
    )

    #set baseline, target and transform config variables
    $baseline_config = "$InstanceDir\$AppName.baseline.config"
    $env_transform = "$InstanceDir\$AppName.Prod.$modifier.config"
    $transform_config = "$InstanceDir\$InstanceName"
    $target_config = "$InstanceDir\$AppName.exe.config"
    write-output "InstanceDir===========$InstanceDir=========="    

	Write-warning "baseline config initially set to $baseline_config"

    #verify instance directory exists
    if (!(Test-Path $InstanceDir -ErrorAction SilentlyContinue)) {
        write-warning "NO SCHEDULED TASK DIRECTORY CURRENTLY CONFIGURED FOR : $InstanceDir"   
        return
    }
    
    #copy package to app instance dir
    Get-ChildItem $WorkingDir -Recurse | foreach {
        Copy-Item -Force -Path $_.FullName -Destination $InstanceDir -Recurse -Container           
    }
    
    Write-output "copied files to $InstanceDir"      
    Write-output "transforming..."
    
    #apply prod environment config
    exec {
        &$Transformer $baseline_config $env_transform $target_config
    }
    
    #apply prod environment instance-specific config
    exec {
        &$Transformer $target_config $transform_config $target_config
    }
        
    Write-Host "Apply transform $transform_config to $baseline_config and output $target_config"    
}


    #if running off staging/qa branch for testing, use prev transformed baseline
    #if(!(test-path "$WorkingDir\$baseline_config" -ErrorAction SilentlyContinue)) {
    #    $baseline_config = "$AppInstallDir\$app_name.exe.config"
    #    Write-warning "baseline config set to $baseline_config"
    #}