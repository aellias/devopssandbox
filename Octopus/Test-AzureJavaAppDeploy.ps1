﻿import-module AZ.websites
import-module Az
try {

    $password = "Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf"
    $securePassword = ConvertTo-SecureString -Force -AsPlainText -String $password
    $username = "devapps@local.imodules.com"
    $credential = New-Object System.Management.Automation.PSCredential ($username , $securePassword)
    $installation_dir = "C:\Octopus\Applications\qawebtools_TentacleV2\QA\email-gateway\1.0.0.017_2"    
    $package_filepath = "C:\test\email-gateway.zip"
    $publish_settings_file = "$installation_dir\azure.publishsettings"
	$sid = "794816cd-45f1-4710-8d31-d019f482cef2"
    
    Connect-Azaccount -Credential $credential
    Get-AzContext -ListAvailable
    Set-AzContext -SubscriptionId $sid
    
    $azure_resourcegroup = "QAEven.Email.Linux"
    $azure_webapp_name = "qaeven-imod-email-gateway"

    Write-Host "step sub id: $sid" 
    Write-Host "package_filepath:   	$package_filepath"
    Write-Host "installation_dir:    	$installation_dir"
    Write-Host "publish_settings_file:  $publish_settings_file"
    Write-Host "azure_resourcegroup:   		$azure_resourcegroup"
    Write-Host "azure_webapp_name:   		$azure_webapp_name"
    Write-Host "subscription_id:    	$sid"
    Write-Host 

    #Import-AzurePublishSettingsFile -PublishSettingsFile $publish_settings_file | Out-Null

    #Stop-AzWebApp -ResourceGroupName $azure_resourcegroup -Name $azure_webapp_name -Verbose
    #Start-Sleep 5
   	
    & az login -u $username -p $password
    & az account set --subscription $sid
	& az webapp deployment source config-zip --resource-group $azure_resourcegroup --name $azure_webapp_name --src $package_filepath --debug


    Start-AzWebApp -ResourceGroupName  $azure_resourcegroup -Name $azure_webapp_name -Verbose
    Write-Host "Published website $azure_webapp_name" -foregroundColor Cyan
}
catch {
    Write-Host "$_.Exception.Message" -foregroundColor Red
    $message = "$_.Exception.Message" | out-file "c:\util\eapi.txt"
    Write-Host "Publish failed for $azure_webapp_name Website"
    throw
}