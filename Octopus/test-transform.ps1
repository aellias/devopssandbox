﻿
function Exec {
    [CmdletBinding()]
    param(
        [Parameter(Position = 0, Mandatory = 1)][scriptblock]$cmd,
        [Parameter(Position = 1, Mandatory = 0)][string]$errorMessage = ($msgs.error_bad_command -f $cmd)
    )
    & $cmd
    if ($lastexitcode -ne 0) {
        throw ("Exec: " + $errorMessage)
    }
}

function Transform-WebConfig {
    param($tenant_name, $env)

    $packages_web = '\\gambit\c$\mercurial\encompass_kanban\Encompass.Web.UI'
    $modifier = ""
    $transform = ""
  	$working_dir = '\\gambit\c$\mercurial\encompass_kanban\Encompass.Web.UI'        
    $transformer = "$working_dir\WebConfigTransformRunner\WebConfigTransformRunner.exe"
    $target = "$working_dir\Web.config"
    
    $baseline_config = "web.config"
    #account for test builds off of staging/release branches with previously transformed configs/no web.config
	if(test-path "$working_dir\Web.baseline.config" -erroraction continue) {
    	$baseline_config = "web.baseline.config"
    }

    #set environment specific variables
    if ($env -match 'production') {
        $modifier = $tenant_name.split('-')[0]
        $transform = "$working_dir\Web.Prod.$modifier.config"
        $cip_transformation = "$working_dir\settings\CloudIdentityProviderConfiguration.Prod.$modifier.config"
    
        #apply prod environment redis transform - only for encompass web
        $releasecolor = $tenant_name.Split("-")[1]
        $redis_transformation = "$working_dir\Web.redis.$releasecolor.config"
        if (test-path $redis_transformation -ErrorAction SilentlyContinue) {
            
            Write-Host "Apply transform $transform to $baseline_config and output $target"
        }        
    } elseif ($env -match 'staging'){
        $modifier = $tenant_name
        $cip_transformation = "$packages_web\settings\CloudIdentityProviderConfiguration.Staging.US.config"
        $transform = "$packages_web\Web.staging.config"
    } else {
        $modifier = $tenant_name
        $cip_transformation = "$packages_web\settings\CloudIdentityProviderConfiguration.QA.$modifier.config"
        $qa_transform = "$packages_web\Web.QA.config"
        $transform = "$working_dir\Web.QA.$modifier.config"
        #apply qa environment config

        Write-Host "Apply transform $qa_transform to $baseline_config and output $target"
    }
		
    write-host "packages_web = $packages_web" 
    write-host "modifier = $modifier" 
    write-host "transform = $transform" 
    write-host "working_dir = $working_dir" 
    write-host "transformer = $transformer" 
    write-host "target = $target" 
    write-host "env = $env" 
    write-host "tenant_name = $tenant_name" 
	set-location $working_dir
    
    #apply environment transform
    Write-Host "Apply transform $transform to $baseline_config and output $target"
        
	#apply cloudidentityprovider transform
    $cip_target = "$working_dir\settings\CloudIdentityProviderConfiguration.config"
    if (test-path $cip_transformation -ErrorAction SilentlyContinue) {
        Write-Host "Copied $cip_transformation to $cip_target"
    }
}


$tenants = 'CAAZ-blue', 'au-blue', 'us-blue', 'staging', 'hotfix', 'even'
foreach($tenant in $tenants) {
    $environment = ''
    if ($tenant -match 'caaz' -or $tenant -match 'au' -or $tenant -match 'us') {
        $environment = 'production'        
        write-host "---------------env = $environment  tenant = $tenant----------------" -ForegroundColor Cyan
        Transform-WebConfig $tenant $environment
    } elseif ($tenant -match 'staging') {
        $environment = 'staging'        
        write-host "---------------env = $environment  tenant = $tenant----------------" -ForegroundColor Cyan
        Transform-WebConfig $tenant $environment
    } else {
        $environment = 'qa'
        write-host "---------------env = $environment  tenant = $tenant----------------" -ForegroundColor Cyan
        Transform-WebConfig -tenant_name $tenant -environment $environment
    }
}