function Transform-Prod-Config {
    if ($OctopusParameters["Octopus.Environment.Name"] -notmatch 'production') {
        return
    }
    
    $NugetInstalled = $false
    if (!(Get-Command nuget -ErrorAction SilentlyContinue)) {
    	cinst nuget.commandline
        Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
        Install-Module psake -force
    	$NugetInstalled = $true
	}
    
    $modifier = ""    
    $nuget = "nuget"
    $tenant_name = $OctopusParameters["Octopus.Deployment.Tenant.Name"]
    $step_name = $OctopusParameters["Octopus.Action.StepName"]
  	$working_dir = $OctopusParameters["Octopus.Action[$step_name].Output.Package.InstallationDirectoryPath"]    
    
	& $nuget install WebConfigTransformRunner	
    
    $transformer = "$working_dir\WebConfigTransformRunner.1.0.0.1\Tools\WebConfigTransformRunner.exe"
    write-host $transformer

    if ($tenant_name -match 'ca') {
        write-host "transforming Prod Configs for $tenant_name"
        $modifier = 'CA'
    } elseif ($tenant_name -match 'us') {
        write-host "transforming Prod Configs for $tenant_name"
        $modifier = 'US'
    } else {
        write-host "transforming Prod Configs for $tenant_name"
        $modifier = 'AU'
    }

    $baseline_config = "web.config"
    $transformation = "$working_dir\Web.Prod.$modifier.config"
    $target = "$working_dir\Web.config"

	set-location $working_dir
    
    #apply prod environment config
    exec {
        &$transformer $baseline_config $transformation $target
    }
    
    Write-Host "Apply transform $transformation to $baseline_config and output $target"
    
    $redis_transformation = "$working_dir\Web.Prod.$modifier.redis.config"
    if (test-path $redis_transformation -ErrorAction SilentlyContinue) {
        #apply prod environment redis config
        exec {
            &$transformer $baseline_config $redis_transformation $target
        }
        Write-Host "Apply transform $transformation to $baseline_config and output $target"
    }
}


