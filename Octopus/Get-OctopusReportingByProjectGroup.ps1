﻿function Get-OctopusReportingByProjectGroup {

    param(
        [parameter(Mandatory = $true)] [string]$project_group,
        [parameter(Mandatory = $true)] [string]$env_name
    )    

    import-module encompass.build -DisableNameChecking -Version 1.2.0.3	

    $d = Get-Date -Format "yyyy-MM--dd-hh-mm-ss"
    $global:logSettings = @{
            logToFile   = $true;
            logFilePath = "C:\git\DevOps\DevopsHelperScripts\Octopus\octopuslogs\octopusreporting.$env_name$d.txt";
    }
    $OctopusURL = "http://octopussand.local.imodules.com:90"
    $header = @{ "X-Octopus-ApiKey" = "API-BD6I4ZJV0VPNLOO6TQTDLET8BE" }
    $environment = ''

    #only include environment specific results for prod/qa and staging
    if('production' -match $env_name) {
        $environment = "Environments-39"
    } elseif ('staging' -match $env_name){
        $environment = "Environments-38"
    } elseif ('qa' -match $env_name){
        $environment = "Environments-22"
    }

    write-log -message "=====GETTING ALL OCTOPUS DEPLOYMENTS FOR $env_name : $environment====="

    #get deployment dump from octopus server
    $json = (Invoke-WebRequest $OctopusURL/api/dashboard/dynamic? -Headers $header -Method Get -UseBasicParsing).content
    $converted = ($json | ConvertFrom-Json).items

    #get name and project id for env tagged projects from OctoSandApiConfig.json
    #project_group param values: Encompass, Email
    $projects = Get-OctopusSandProjects -project_group $project_group | ForEach-Object -Process {
        $config_proj_id = $_.Project
        $config_name = $_.Name

        #check each deployment record in converted deployment dump for matching project id and environment
        $converted | ForEach-Object -Process {  
            $deployed_proj_id = $_.ProjectId
            $release_version = $_.ReleaseVersion
            $state = $_.State
            $completed_time = $_.CompletedTime.split('.')[0]
            $environment_id = $_.EnvironmentId
                        
            if(($deployed_proj_id -eq $config_proj_id) -and ($environment_id -eq $environment)){
                write-log @"
$config_name version $release_version deployed
TIME COMPLETED: $completed_time
STATE: $state
"@
            }

        }
    }

    remove-module encompass.build

}


#proj groups : encompass, email
#env names: staging, qa, prod
Get-OctopusReportingByProjectGroup -project_group 'Encompass' -env_name 'prod'