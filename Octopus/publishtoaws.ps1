try
{
$AwsAccessKey= $OctopusParameters["AwsAccessKey"]
$AwsSecretKey= $OctopusParameters["AwsSecretKey"]
$Bucket= $OctopusParameters["Bucket"]
$ExecutableName= $OctopusParameters["ExecutableName"]
$AWSApplicationName= $OctopusParameters["AWSApplicationName"]
$AWSEnvironmentId= $OctopusParameters["AWSEnvironmentId"]
$VersionNumber= $OctopusParameters["VersionNumber"]
$Description= $OctopusParameters["Description"]
$AwsRegion= $OctopusParameters["AwsRegion"]
#$DeploymentFolder= $OctopusParameters["DeploymentFolder"]


$ErrorActionPreference = "Stop"
$credentialsProfileName = "imodservice"
$keyprefix = "$AWSApplicationName/$VersionNumber/";
$filename = "$ExecutableName"
$filepath = "$keyprefix$DeploymentFolder"

#print info
write-host "awsaccesskey = $AwsAccessKey"
write-host "AwsSecretKey = $AwsSecretKey"
write-host "Bucket = $Bucket"
write-host "DeploymentFolder = $DeploymentFolder"
write-host "ExecutableName = $ExecutableName"
write-host "AWSApplicationName = $AWSApplicationName"
write-host "AWSEnvironmentId = $AWSEnvironmentId"
write-host "VersionNumber = $VersionNumber"
write-host "Description = $Description"
write-host "AwsRegion = $AwsRegion"
write-host "keyprefix = $keyprefix"
write-host "filename = $filename"
write-host "filepath = $filepath"


Write-Output "`nSetting AWS credentials...`n"
Set-AWSCredentials -AccessKey $AwsAccessKey -SecretKey $AwsSecretKey -StoreAs $credentialsProfileName
Set-AWSCredentials -ProfileName $credentialsProfileName

Write-Output "Checking if application version exists...`n"
$checkVersion = Get-EBApplicationVersion -ApplicationName "$AWSApplicationName" -VersionLabel $VersionNumber -Region $AwsRegion;

if($checkVersion.length -eq 0){        
    Write-Output "Application version $VersionNumber does not exist.`n"

    Write-Output "Uploading files to S3 bucket...`n"
    #Write-S3Object -BucketName $Bucket -KeyPrefix $keyprefix -Folder $DeploymentFolder -ErrorAction stop
    Write-S3Object -BucketName $Bucket -Key "$filepath" -File "$DeploymentFolder/$filename" -ErrorAction stop

    Write-Output "Creating application version...`n"
    New-EBApplicationVersion `
    -ApplicationName "$AWSApplicationName" `
    -VersionLabel $VersionNumber `
    -Description $Description `
    -AutoCreateApplication $false `
    -SourceBundle_S3Bucket $Bucket `
    -SourceBundle_S3Key "$filepath" `
    -Region $AwsRegion  
} else{
    Write-Output "Application version $VersionNumber exists. Skipping file upload and application version creation..."
}


Write-Output "`nChecking if environment is ready for updating...`n"
$AWSEnvironment = Get-EBEnvironment -ApplicationName "$AWSApplicationName" -EnvironmentId $AWSEnvironmentId -Region $AwsRegion;
$checkLimit = 0;
while($AWSEnvironment.Status.Value -ne 'Ready'){
    $checkLimit++;
    if ($checkLimit -eq 5){
        throw "AWS was not ready for the maximum amount of tries."
    }

    Write-Output "Environment is not ready. Current Status: $($AWSEnvironment.Status.Value). Will retry in 10 seconds...`n";
    sleep 10;

    $AWSEnvironment = Get-EBEnvironment -ApplicationName "$AWSApplicationName" -EnvironmentId $AWSEnvironmentId -Region $AwsRegion;
}


Write-Output "Environment is ready! *<|:-)`n"
Write-Output "Updating Elastic Beanstalk environment to version $VersionNumber...`n"
Update-EBEnvironment `
    -EnvironmentId $AWSEnvironmentId `
    -VersionLabel $VersionNumber `
    -Region $AwsRegion

Write-Output "`nClearing stored AWS credentials...`n"

Clear-AWSCredentials -StoredCredentials $credentialsProfileName

Write-Output "All done!"
}
catch {
    Write-Host "$_.Exception.Message" -foregroundColor Red
    $message = "$_.Exception.Message" | out-file "c:\util\eapi.txt"
    Write-Host "Publish failed for $AWSApplicationName Website"
    throw
}