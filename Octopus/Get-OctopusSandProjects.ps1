function Get-OctopusSandProjects {
    param($project_group)

    if($project_group){        
        Write-Host "Get-OctopusSchema: $PSScriptRoot\lib\OctoSandApiConfig.json"
        $octopusSchemaAll = Get-Content $PSScriptRoot\lib\OctoSandApiConfig.json -Raw | ConvertFrom-Json 
        $octopusSchema = $octopusSchemaAll.ProjectGroups.$project_group
        return $octopusSchema
    }

    Write-Host "Get-OctopusSchema: $PSScriptRoot\lib\OctoSandApiConfig.json"
    $octopusSchema = Get-Content $PSScriptRoot\lib\OctoSandApiConfig.json -Raw | ConvertFrom-Json 
    return $octopusSchema
}

