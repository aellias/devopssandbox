﻿$secpasswd = ConvertTo-SecureString 'Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf' -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ("devapps", $secpasswd)
$dev_machines = "MYSTIQUE", "STORM", "HAVOK", "PSYLOCKE", "JUGGERNAUT", "BISHOP", "SABRETOOTH", "ROGUE", "CYCLOPS", "BEAST", "COLOSSUS"
foreach ($machine in $dev_machines){
    try {
        Enter-PSSession -ComputerName $machine -Credential $cred   
        Get-service 'OctopusDeploy Tentacle'
        write-host "$machine session"
        Exit-PSSession
    } catch {
        write-host $machine
    }    
}