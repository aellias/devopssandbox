﻿$Apiversion = '2015-08-01'
$password = "Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf"
$securePassword = ConvertTo-SecureString -Force -AsPlainText -String $password
$username = "devapps@local.imodules.com"
$credential = New-Object System.Management.Automation.PSCredential ($username , $securePassword)
    
$azure_resourcegroup = 'emstaging'
$azure_webapp_name = 'DeliveryConsumers-staging'
$azure_webjob_name = 'DeliveryConsumers-staging-job'
$apiUrl = "https://$azure_webapp_name.scm.azurewebsites.net/api/continuouswebjobs/$azure_webjob_name"

function Get-PublishingProfileCredentials($azure_resourcegroup, $azure_webapp_name){

        $resourceType = "Microsoft.Web/sites/config"
        $resourceName = "$azure_webapp_name/publishingcredentials"
        $publishingCredentials = Invoke-AzResourceAction -resourceGroupName $azure_resourcegroup -ResourceType $resourceType -ResourceName $resourceName -Action list -ApiVersion $Apiversion -Force
        return $publishingCredentials
}

#Pulling authorization access token :
function Get-KuduApiAuthorisationHeaderValue($azure_resourcegroup, $azure_webapp_name){
    $publishingCredentials = Get-PublishingProfileCredentials $azure_resourcegroup $azure_webapp_name
    return ("Basic {0}" -f [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f 
    $publishingCredentials.Properties.PublishingUserName, $publishingCredentials.Properties.PublishingPassword))))
}

$accessToken = Get-KuduApiAuthorisationHeaderValue $azure_resourcegroup $azure_webapp_name
    
#Generating header to create and publish the Webjob :
$Header = @{
    'Content-Disposition'='attachment; attachment; filename=Copy.zip'
    'Authorization'=$accessToken
}

$result = Invoke-RestMethod -Uri $apiUrl -Headers $Header -Method get
write-host $result




<#


$sidcaaz = 'd8b45a99-36d1-4f1a-9962-e257faf33336'
$sid = '2a31a90f-b189-4af6-88e2-03e091e95c25'

function SetAzureSubscription($publishSettingsFile, $subscriptionId ) {
    
    Import-AzurePublishSettingsFile -PublishSettingsFile $publishSettingsFile | Out-Null
    Select-AzureSubscription -SubscriptionId $subscriptionId | Out-Null
}

Get-AzureRmResourceGroup | Get-AzureRmResourceGroupDeployment


Connect-Azaccount -Credential $credential
Get-AzContext -ListAvailable
Set-AzContext -SubscriptionId $sid

Get-AzResourceGroup -Name $resourceGroup | Get-AzResourceGroupDeployment | out-file "C:\git\DevOps\DevopsHelperScripts\Octopus\caazure.txt"

$resources = Get-AzResourceGroup -Name $resourceGroup | Get-AzResourceGroupDeployment

write-host $resources.ParametersString

#>