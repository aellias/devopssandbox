

function Get-Sites($siteidlist, $dbsingleservername) {
  Write-Host "Starting task Get-Sites"
  
  $query = "select site_info_id,site_name,db_server,db_catalog from site_info where is_disabled = 0 and site_name != '_iModMaster Access'"
  if ($siteidlist)
  {
    $query = "select site_info_id,site_name,db_server,db_catalog from site_info where is_disabled = 0 and site_info_id in ($siteidlist) and site_name != '_iModMaster Access'"    
  }
  
  if($dbsingleservername)
  {
    Write-Host "DbSingle Server is called" $dbsingleservername  
    $query = "select site_info_id,site_name,db_server,db_catalog from site_info where is_disabled = 0 and db_server = '$dbsingleservername' and site_name != '_iModMaster Access'"    
    Write-Host "The query we are going to run is " $query
  }
  
  $DatabaseName = 'imod_master'
  $connectionString = "Server=$ServerName;Database=$DatabaseName;User Id=$UserName;Password=$Password;"
  $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
  $command = new-object system.data.sqlclient.sqlcommand($query,$connection)
  $connection.Open()

  $reader = $command.ExecuteReader()

  $results = @()
  while ($reader.Read())
    {
        $row = @{}
        for ($i = 0; $i -lt $reader.FieldCount; $i++)
        {
            $row[$reader.GetName($i)] = $reader.GetSqlValue($i)
        }
        $results += new-object psobject -property $row            
    }
    $connection.Close();

    $results

}

function Run-Site-Catalog($dbserver, $dbcatalog, $siteidinfo) {
  
  $timestamp = get-date -Format T
  Write-Host "$timestamp Starting task Run-Site-Catalog for $siteidinfo $dbserver.$dbcatalog"
  #$scriptfile = "C:\BuildServer\SiteCatalogs_ROLL2.sql"
    
  $connectionString = "Server=$dbserver;Database=$dbcatalog;User Id=$UserName;Password=$Password;"
  $query = [IO.File]::ReadAllText($scriptfile)
  $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
  $command = new-object system.data.sqlclient.sqlcommand($query,$connection)
  try{
      $connection.Open()
      $content = [IO.File]::ReadAllText($scriptfile)
      $queries = [System.Text.RegularExpressions.Regex]::Split($query, "^\s*GO\s*`$", [System.Text.RegularExpressions.RegexOptions]::IgnoreCase -bor [System.Text.RegularExpressions.RegexOptions]::Multiline)
    
        $queries | ForEach-Object {
            $q = $_
            if ((-not [String]::IsNullOrWhiteSpace($q)) -and ($q.Trim().ToLowerInvariant() -ne "go")) {            
                $command = $connection.CreateCommand()
                $command.CommandText = $q
                $command.CommandTimeout = $commandtimeout
                $command.ExecuteNonQuery() | Out-Null
                Write-host $command
            }
        }
  }
  catch{
    if ($OctopusParameters['ContinueOnError']) {
		Write-Warning $_.Exception.Message 
	}
	else {
		throw
	}
  }
  finally{
      $connection.Close();
      Write-Host "Closing connection"
  }
  
  $timestamp = get-date -Format T
  Write-Host "$timestamp Finished task Run-Site-Catalog for $siteidinfo $dbserver.$dbcatalog"

}


function Execute-Site-Catalog-Script($site_id_list, $db_single_server_name){

$timestamp = get-date -Format T
$sites = Get-Sites $site_id_list $db_single_server_name
Write-Host "$timestamp  ============== Starting task Execute-Site-Catalog-Script for $environment environment ============== "
foreach ($site in $sites){

  Run-Site-Catalog $site.db_server $site.db_catalog $site.site_info_id

  } 
 Write-Host "$timestamp ============== Finished task Execute-Site-Catalog-Script for $environment environment ============== " 
}

$environment = 'qa'
$db_single_server_name = ''
$site_id_list = "7,12,17,22,27,32,39,44,49,54,67,80,89"
$scriptfile = 'C:\mercurial\encompass_kanban\BuildArtifacts\packages\sql\SiteCatalogs_ROLL.sql'
$UserName = 'ScriptRunnerQA'
$Password = 'UTA4rFux81zAbUiGa2jF'
$ServerName = 'qasqlmaster'
$commandtimeout = '30'


Execute-Site-Catalog-Script $site_id_list $db_single_server_name