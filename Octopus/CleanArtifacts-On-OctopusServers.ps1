function CleanArtifacts-On-OctopusServers {
   
    $artifacts_root_dir = 'c:\OctopusData\Packages'


    Set-Location -Path $artifacts_root_dir 
    $exclude = '*RedisFlushDatabase*'
    $projects = Get-Childitem -path $artifacts_root_dir
    foreach ($project in $projects) {
        Get-Childitem -path $project.Fullname -Exclude $exclude  | Foreach-Object {if ($_.LastWriteTime -le (get-date).adddays(-12)) {
            remove-item -force -path $_.Fullname
            Write-host $_.Fullname
            }
        }
    }
    
    
    Write-Host "deleted"
}
CleanArtifacts-On-OctopusServers