$baseconfig = "$PSScriptRoot\Transform-ProdAppConfig.ps1"
$specialcases = "$PSScriptRoot\Transform-ProdTestAppConfig.ps1"
.$baseconfig
.$specialcases

#only run in prod env.
if ($OctopusParameters["Octopus.Environment.Name"] -notmatch 'production') {
        return
}

#set variables and install necessary dependencies  
$app_name = $OctopusParameters["AppName"]
$app_install_dir = $OctopusParameters["app.install.dir"]
$step_name = $OctopusParameters["Octopus.Action.StepName"]
$working_dir = $OctopusParameters["Octopus.Action[$step_name].Output.Package.InstallationDirectoryPath"]    

$NugetInstalled = $false
if (!(Get-Command nuget -ErrorAction SilentlyContinue)) {
    cinst nuget.commandline
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
    Install-Module psake -force
    $NugetInstalled = $true
} 
$nuget = "nuget"
& $nuget install WebConfigTransformRunner	
$webtransformer = "$working_dir\WebConfigTransformRunner.1.0.0.1\Tools\WebConfigTransformRunner.exe"


#get environment modifier
$modifier = ""   
$tenant_name = $OctopusParameters["Octopus.Deployment.Tenant.Name"]
if ($tenant_name -match 'caaz') {
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'CAAZ'
} elseif ($tenant_name -match 'ca') {
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'CA'
} elseif ($tenant_name -match 'us') {
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'US'
} elseif ($tenant_name -match 'au') {
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'AU'
} else {
    write-host "transforming Prod Configs for $tenant_name -UNKNOWN"
    $modifier = ''
}

$appinstances = get-childitem "$working_dir\$app_name.prod.$modifier.*.config"

if ($appinstances -ne $null) {
    write-Output "Transform baseline config and copy to WaveTest, DBs and RC instances"
    foreach ($app in $appinstances) {
        write-output "=====================================$app_name================================================"
        #name = CommunityImportTool.Prod.CAAZ.WaveTest.config, CommunityImportTool.Prod.CAAZ.RC.config
        $name = $app.name
        $replace1 = $name -replace ".Prod.$modifier.", "_"
        $replace2 = $replace1 -replace ".config", ""
        $instance_dir = "$app_install_dir\$replace2"
        Transform-AndCopyProdAppInstance -AppName $app_name -AppInstallDir $app_install_dir -Transformer $webtransformer -WorkingDir $working_dir -InstanceDir $instance_dir -InstanceName $name
    }
} else {
    write-output "$app_name only has one instance in $modifier environment"
}

#transform base application config
Transform-ProdAppConfig -AppName $app_name -Transformer $webtransformer -WorkingDir $working_dir