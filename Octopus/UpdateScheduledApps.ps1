#only run in prod env.
if ($OctopusParameters["Octopus.Environment.Name"] -notmatch 'production') {
        return
}

#set variables and install necessary dependencies  
$app_name = $OctopusParameters["AppName"]
$step_name = $OctopusParameters["Octopus.Action.StepName"]
$working_dir = $OctopusParameters["Octopus.Action[$step_name].Output.Package.InstallationDirectoryPath"]    
$apps_folder = $OctopusParameters["app.install.dir"]

<# added transformer to package in tc
$NugetInstalled = $false
if (!(Get-Command nuget -ErrorAction SilentlyContinue)) {
    cinst nuget.commandline
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
    Install-Module psake -force
    $NugetInstalled = $true
} 
$nuget = "nuget"
& $nuget install WebConfigTransformRunner
#>

$webtransformer = "$working_dir\WebConfigTransformRunner\WebConfigTransformRunner.exe"

#get environment modifier
$modifier = ""   
$tenant_name = $OctopusParameters["Octopus.Deployment.Tenant.Name"]
if ($tenant_name -match 'caaz') {
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'CAAZ'
} elseif ($tenant_name -match 'ca') {
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'CA'
} elseif ($tenant_name -match 'us') {
	if ($step_name -match 'Scheduled Emails'){
    	$apps_folder = 'E:\Apps'
    } else {
    	$apps_folder = $OctopusParameters["app.install.dir"]
    }
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'US'
} elseif ($tenant_name -match 'au') {
    write-host "transforming Prod Configs for $tenant_name"
    $modifier = 'AU'
} else {
    write-host "transforming Prod Configs for $tenant_name -UNKNOWN"
    $modifier = ''
}

write-output "workingDir===========$workingDir=========="
write-Output "Transform baseline config and copy to WaveTest, DBs and RC instances"
$appinstances = get-childitem "$working_dir\$app_name.prod.$modifier.*.config"

if ($appinstances -ne $null) {
    foreach ($app in $appinstances) {
        write-output "=====================================$app_name================================================"
        #name = CommunityImportTool.Prod.CAAZ.WaveTest.config, CommunityImportTool.Prod.CAAZ.RC.config
        $name = $app.name
        $replace1 = $name -replace ".Prod.$modifier.", "_"
        $name_replace = $replace1 -replace ".config", ""
        $instance_dir = "$apps_folder\$replace2"
        Transform-AndCopyProdAppInstance -AppName $app_name -Transformer $webtransformer -WorkingDir $working_dir -InstanceDir $instance_dir -InstanceName $name -modifier $modifier
    }
} else {
    write-output "$app_name only has one instance in $modifier environment"
}

#transform base application config
Transform-ProdAppConfig -AppName $app_name -Transformer $webtransformer -WorkingDir $working_dir -modifier $modifier