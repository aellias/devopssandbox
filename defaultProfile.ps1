
<#PSScriptInfo
.VERSION 1.0.2.0
.GUID ff662588-100a-4d7b-a6f8-b7d881186289
.AUTHOR rellias
.COMPANYNAME iModules Software Inc
.COPYRIGHT (c) 2018 iModules Software Inc. All rights reserved.
.TAGS Profile
.EXTERNALMODULEDEPENDENCIES 
.LICENSEURI
.PROJECTURI
.ICONURI
.EXTERNALMODULEDEPENDENCIES
.REQUIREDSCRIPTS
.EXTERNALSCRIPTDEPENDENCIES
.RELEASENOTES
.DESCRIPTION 
Powershell Profile For Developer Machines
#> 

Push-Location (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent)

$theHost = Get-Host
$me = [Environment]::UserName
Write-Host "Loading $me profile. Host: $($theHost.Name)" -ForegroundColor green
write-host

$identity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$principal = new-object System.Security.Principal.WindowsPrincipal($identity)
$adm = [System.Security.Principal.WindowsBuiltInRole]::Administrator
$IsAdmin = $principal.IsInRole($adm)

$devconfig_filepath = [Environment]::GetEnvironmentVariable("DEVCONFIG", "Machine")
if (!(Test-Path $devconfig_filepath)) {
    write-host "Missing DevConfig.json" -ForegroundColor Red
}

$devconfig = Get-Content $devconfig_filepath -Raw -ErrorAction:SilentlyContinue -WarningAction:SilentlyContinue | ConvertFrom-Json -ErrorAction:SilentlyContinue -WarningAction:SilentlyContinue
Write-Host "Loaded DevConfig from: $devconfig_filepath" -ForegroundColor green
Write-Host "`tTo edit in VsCode use command devconfig"
write-host

$ProgramFiles86 = ${env:ProgramFiles(x86)}
$ProgramFiles = ${env:ProgramFiles}
function cdenc {  cd $devconfig.EncompassKanbanDir }
set-alias enc cdenc
function cdcore { cd $devconfig.EncompassCoreDir }
set-alias core cdcore
function cddevops { cd "C:\git\DevOps"; }
set-alias dop cddevops
function cdemsrv { cd $devconfig.EmailServicesDir }
set-alias emsrv cdemsrv
function cdestw { cd "$($devconfig.GitRootDir)\encompass-support-tools-website" }
set-alias estw cdestw

function Get-DevAppsCredentials {
    $global:DevAppsCred = Get-PSCredential -Username $devconfig.Credentials.DevApps.Username -PassWord $devconfig.Credentials.DevApps.Password
}

function Get-DevappsAzureCredentials {
    $global:DevAppsAzureCred = Get-PSCredential -Username $devconfig.Credentials.DevappsAzure.Username -PassWord $devconfig.Credentials.DevappsAzure.Password
}

function Get-PSCredential {
    param (
        [string]$Username,
        [string]$PassWord
    )
    $SecurePassWord = ConvertTo-SecureString -AsPlainText $Password -Force
    $Credential = New-Object System.Management.Automation.PSCredential ($Username, $SecurePassWord)
    return $Credential
}

function Start-Ahk {        
    $ahkExe = 'C:\PortableTools\AutoHotkey\bin\AutoHotkey.exe'
    $arg1 = 'C:\PortableTools\AutoHotkey\Startup.ahk'
    & $ahkExe $arg1 
}
Set-Alias ahk Start-Ahk
function Start-Logitech {    
    
    $LogitechExe = "C:\Program Files\Logitech Gaming Software\LCore.exe"
    & $LogitechExe
}
Set-Alias logi Start-Logitech
function editDevconfig {
    code $devconfig_filepath 
}
set-alias devconfig editDevconfig

$userModules = $devconfig.PsModulesDir
$userFunctions = $devconfig.PsAutoLoadDir

if ( !(($env:PSModulePath).split(";") -contains $userModules) ) {
    $env:PSModulePath = $userModules + ";" + $env:PSModulePath
    Write-host "Custom Modules path inserted: $userModules" -ForegroundColor green
    write-host
    #$env:PSModulePath
}
if (test-path $userFunctions) {
    Get-ChildItem $userFunctions -recurse -filter "*.ps1" | % {.$_.fullname}
    Write-Host "Custom functions loaded from: $userFunctions" -ForegroundColor green
}

Import-Module Encompass.DevOps -DisableNameChecking
Import-Module Encompass.Build -DisableNameChecking
function Show-TasksMenuForCurrentLocation {
    $here = Get-Location
    $tasks_file = "$here\_build\tasks.json"
    
    if ($tasks_file) {
        Show-TaskMenu -menu_config_filepath $tasks_file -working_dir $here
    }
    else {
        Write-Host "No Tasks.json file found in any sub-directory" -ForegroundColor RED
    }
}
set-alias menu Show-TasksMenuForCurrentLocation

$ImportedHistoryCount = 0
$HistoryDirPath = $devconfig.PsHistory
$HistoryFileName = "history.txt"
$HistoryFilePath = "$($devconfig.PsHistory)\$HistoryFileName"
write-host "Loaded history from: $HistoryFilePath" -ForegroundColor green
write-host "`tTo see all history in a grid use F7"
write-host

write-host "Show Key Bindings: Ctrl+Alt+Shift+?" -ForegroundColor green
write-host

# import-module PSReadline you have to define the prompt & do history stuff before importing this
import-module PSReadline -ErrorAction:SilentlyContinue -WarningAction:SilentlyContinue

Set-PSReadlineKeyHandler -Key Tab -Function MenuComplete
Set-PSReadlineOption -ShowToolTips
Set-PSReadLineOption -HistoryNoDuplicates
Set-PSReadLineOption -HistorySearchCursorMovesToEnd
Set-PSReadLineOption -HistorySavePath $HistoryFilePath

Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

Set-PSReadlineKeyHandler -Key Alt+Backspace -Function ShellBackwardKillWord
Set-PSReadlineKeyHandler -Key Alt+B -Function ShellBackwardWord
Set-PSReadlineKeyHandler -Key Alt+F -Function ShellForwardWord


# Insert text from the clipboard as a here string
Set-PSReadlineKeyHandler -Key Ctrl+Shift+v `
    -BriefDescription PasteAsHereString `
    -LongDescription "Paste the clipboard text as a here string" `
    -ScriptBlock {
    param($key, $arg)

    Add-Type -Assembly PresentationCore
    if ([System.Windows.Clipboard]::ContainsText()) {
        # Get clipboard text - remove trailing spaces, convert \r\n to \n, and remove the final \n.
        $text = ([System.Windows.Clipboard]::GetText() -replace "\p{Zs}*`r?`n", "`n").TrimEnd()
        [Microsoft.PowerShell.PSConsoleReadLine]::Insert("@'`n$text`n'@")
    }
    else {
        [Microsoft.PowerShell.PSConsoleReadLine]::Ding()
    }
}

# F1 for help on the command line - naturally
Set-PSReadlineKeyHandler -Key F1 `
    -BriefDescription CommandHelp `
    -LongDescription "Open the help window for the current command" `
    -ScriptBlock {
    param($key, $arg)

    $ast = $null
    $tokens = $null
    $errors = $null
    $cursor = $null
    [Microsoft.PowerShell.PSConsoleReadLine]::GetBufferState([ref]$ast, [ref]$tokens, [ref]$errors, [ref]$cursor)

    $commandAst = $ast.FindAll( {
            $node = $args[0]
            $node -is [System.Management.Automation.Language.CommandAst] -and
            $node.Extent.StartOffset -le $cursor -and
            $node.Extent.EndOffset -ge $cursor
        }, $true) | Select-Object -Last 1

    if ($commandAst -ne $null) {
        $commandName = $commandAst.GetCommandName()
        if ($commandName -ne $null) {
            $command = $ExecutionContext.InvokeCommand.GetCommand($commandName, 'All')
            if ($command -is [System.Management.Automation.AliasInfo]) {
                $commandName = $command.ResolvedCommandName
            }

            if ($commandName -ne $null) {
                Get-Help $commandName -ShowWindow
            }
        }
    }
}

Set-PSReadlineKeyHandler -Key F7 -BriefDescription History -LongDescription 'Show history' -ScriptBlock {
    $r = [System.Collections.ArrayList](Get-Content (Get-PSReadlineOption).HistorySavePath)
    $r.Reverse()
    $r = $r | Out-GridView -PassThru
    if (!$r) {
        return
    }
    [Microsoft.PowerShell.PSConsoleReadLine]::RevertLine()
    [Microsoft.PowerShell.PSConsoleReadLine]::Insert($r)
}

# Ctrl+Alt+j then type a key to mark the current directory.
# Alt+J to show the current shortcuts in a popup
# Ctrj+j then the same key will change back to that directory without
# needing to type cd and won't change the command line.
 
#pre-populate a global variable
$global:PSReadlineMarks = @{
    [char]"1" = $devconfig.EncompassKanbanDir
    [char]"2" = 'C:\mercurial\encompass_core'
    [char]"3" = 'C:\git\DevOps\powershellmodules'
    [char]"4" = 'C:\git\editor-api'
    [char]"5" = 'C:\git\list-management'
    [char]"6" = 'C:\git\scoreboards'
    [char]"7" = 'C:\git\email-services'
    [char]"8" = 'C:\git\email-services\applications\email-gateway'
}
 
Set-PSReadlineKeyHandler -Key Ctrl+Alt+j -BriefDescription MarkDirectory -LongDescription "Mark the current directory. [$($env:username)]" -ScriptBlock {
 
    #press a single character to mark the current directory
    $key = [Console]::ReadKey($true)
    if ($key.keychar -match "\w") {
        $global:PSReadlineMarks[$key.KeyChar] = $pwd
    }
    else {
        [Microsoft.PowerShell.PSConsoleReadLine]::Ding()
        Write-Warning "You entered an invalid character."
        [Microsoft.PowerShell.PSConsoleReadLine]::AcceptLine()
    }
}
 
Set-PSReadlineKeyHandler -Key Ctrl+j -BriefDescription JumpDirectory -LongDescription "Goto the marked directory.[$($env:username)]" -ScriptBlock {
 
    $key = [Console]::ReadKey()
    $dir = $global:PSReadlineMarks[$key.KeyChar]
    if ($dir) {
        cd $dir
        [Microsoft.PowerShell.PSConsoleReadLine]::InvokePrompt()
    }
}
 
Set-PSReadlineKeyHandler -Key Alt+j -BriefDescription ShowDirectoryMarks -LongDescription "Show the currently marked directories in a popup. [$($env:username)]" -ScriptBlock {
 
    $data = $global:PSReadlineMarks.GetEnumerator() | Where {$_.key} | Sort key 
    $data | foreach -begin {
        $text = @"
Key`tDirectory
---`t---------
 
"@
    } -process { 
     
        $text += "{0}`t{1}`n" -f $_.key, $_.value
    } 
    $ws = New-Object -ComObject Wscript.Shell
    $ws.popup($text, 10, "Use Ctrl+J to jump") | Out-Null
     
    [Microsoft.PowerShell.PSConsoleReadLine]::InvokePrompt()
   
}

#[Environment]::SetEnvironmentVariable("JAVA_HOME", "C:\Program Files\Java\jdk1.8.0_191", [System.EnvironmentVariableTarget]::Process)

Import-Module posh-hg
if ($devconfig.PsPrompt.UseVcsPrompt -ne 'false'){
    Add-PoshGitToProfile -StartSshAgent -WarningAction:SilentlyContinue
}
if ($IsAdmin) {
    Import-Module WebAdministration
}
Import-Module "$userModules\BuildUtilities\AssemblyBindingUtilities.dll"
function prompt {
    

    $realLASTEXITCODE = $LASTEXITCODE
    
    if ($devconfig.PsPrompt.UseShortPath -eq 'true') {
        Write-Host $((Get-Location | Split-Path -leaf).ToLower()) -nonewline
    }
    else {
        Write-Host (get-location) -nonewline
    }
    
    
    if ($devconfig.PsPrompt.UseVcsPrompt -ne 'false') {
        Write-VcsStatus
    }
    
    $global:LASTEXITCODE = $realLASTEXITCODE
    return "> "
}

Pop-Location
