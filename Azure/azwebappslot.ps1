import-module Az

$password = "Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf"
$securePassword = ConvertTo-SecureString -Force -AsPlainText -String $password
$username = "devapps@local.imodules.com"
$credential = New-Object System.Management.Automation.PSCredential ($username , $securePassword)
$installation_dir = "C:\devops\octopuspackages"
$package_filepath = "C:\devops\octopuspackages\EditorApi.1.1.0.01628.zip"
$publish_settings_file = "$installation_dir\azure.publishsettings"
$sid = "2a31a90f-b189-4af6-88e2-03e091e95c25"
$azure_resourcegroup = "emqa"
$azure_webapp_name = "TemplateBuilder-qa-demo"
$slot_name = "TemplateBuilder-qa-demo-1"

write-host 'connecting'
Connect-Azaccount -Credential $credential
write-host 'connected'
Get-AzContext -ListAvailable
Set-AzContext -SubscriptionId $sid

Get-AzWebAppSlot -ResourceGroupName $azure_resourcegroup -name $azure_webapp_name

Publish-AzWebApp -Name $azure_webapp_name -ResourceGroupName $azure_resourcegroup -slot $slot_name -ArchivePath $package_filepath -force -ErrorAction Stop -Verbose




<#
Stop-AzWebApp -ResourceGroupName $azure_resourcegroup -Name $azure_webapp_name -Verbose
Start-Sleep 5
    
#delete files in wwwroot to force clean deploy
#Clean-AzureWebAppDir -WebAppName $azure_webapp_name -ResourceGroup $azure_resourcegroup -credential $credential

Publish-AzWebApp -Name $azure_webapp_name -ResourceGroupName $azure_resourcegroup -ArchivePath $package_filepath -force -ErrorAction Stop -Verbose  | Out-Null

Start-AzWebApp -ResourceGroupName  $azure_resourcegroup -Name $azure_webapp_name -Verbose

#>
#New-AzWebAppSlot -ResourceGroupName $azure_resourcegroup -name $azure_webapp_name -slot staging