﻿<#
import-module encompas.octopusdeploy

$projids = "Projects-162", "Projects-163", "Projects-166", "Projects-167", "Projects-168", "Projects-169"

$repo = Get-OctopusRepository

foreach ($id in $projids) {
    Create-OctopusProjectChannels -repository $repo -projectId $id
}
#>

import-module encompass.octopusdeploy

###Config###

$OctopusAPIKey = "API-BD6I4ZJV0VPNLOO6TQTDLET8BE" #API key to authenticate with the Octopus API
$RoleFilter = "" #Role you want to filter for.

###Process###

$OctopusBaseURL = "http://octopussand.local.imodules.com:90/"

$EnvironmentID = "Environments-22"

$header = @{ "X-Octopus-ApiKey" = $OctopusAPIKey }

$AllMachines = (Invoke-WebRequest $OctopusBaseURL/api/environments/$EnvironmentID/machines -Headers $header | ConvertFrom-Json).items

#$Machines = $AllMachines |?{$_.roles -icontains $RoleFilter}

$AllMachines | out-file C:\DevOps\qamachines.txt

remove-module encompas.octopusdeploy