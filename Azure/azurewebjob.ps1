﻿import-module Az

try {

	#Resource details :
	$Apiversion = '2015-08-01'
	$password = "Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf"
    $securePassword = ConvertTo-SecureString -Force -AsPlainText -String $password
    $username = "devapps@local.imodules.com"
    $credential = New-Object System.Management.Automation.PSCredential ($username , $securePassword)
    
    $package_file = $OctopusParameters["Octopus.Action[Deploy Package To Jumpserver].Output.package.file"]    
    $installation_dir = $OctopusParameters["Octopus.Action[Deploy Package To Jumpserver].Output.Package.InstallationDirectoryPath"]
    $publish_settings_file = "$installation_dir\azure.publishsettings"
    
    $subscription_id = $OctopusParameters["st.subscription_id"]
    $azure_webapp_name = $OctopusParameters["templatebuilder-qa-odd"]
    $azure_webjob_name = $OctopusParameters["templatebuilder-qa-odd"]
    $azure_resourcegroup = $OctopusParameters["emqa"]
    
    $apiUrl = "https://$azure_webapp_name.scm.azurewebsites.net/api/continuouswebjobs/$azure_webjob_name"
    write-host $apiUrl
    
    Write-Host "project:   	$OctopusParameters['Octopus.Project.Name']"
    Write-Host "package_file:   	$package_file"
    Write-Host "installation_dir:    	$installation_dir"
    Write-Host "publish_settings_file:  $publish_settings_file"
    Write-Host "resourceGroup:   		$azure_resourcegroup"
    Write-Host "azure.webapp.name:   		$azure_webapp_name"
    Write-Host "azure.webjob.name:   		    $azure_webjob_name"
    Write-Host "subscription_id:    	$subscription_id"
    
    #Function to get Publishing credentials for the WebApp :
    function Get-PublishingProfileCredentials($azure_resourcegroup, $azure_webapp_name){

        $resourceType = "Microsoft.Web/sites/config"
        $resourceName = "$azure_webapp_name/publishingcredentials"
        $publishingCredentials = Invoke-AzResourceAction -resourceGroupName $azure_resourcegroup -ResourceType $resourceType -ResourceName $resourceName -Action list -ApiVersion $Apiversion -Force
        return $publishingCredentials
    }

    #Pulling authorization access token :
    function Get-KuduApiAuthorisationHeaderValue($azure_resourcegroup, $azure_webapp_name){
        $publishingCredentials = Get-PublishingProfileCredentials $azure_resourcegroup $azure_webapp_name
        return ("Basic {0}" -f [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f 
        $publishingCredentials.Properties.PublishingUserName, $publishingCredentials.Properties.PublishingPassword))))
    }
    
    Connect-Azaccount -Credential $credential
    Set-AzContext -SubscriptionId $subscription_id
	$accessToken = Get-KuduApiAuthorisationHeaderValue $azure_resourcegroup $azure_webapp_name
    
    #Generating header to create and publish the Webjob :

    $Header = @{
        'Content-Disposition'='attachment; attachment; filename=Copy.zip'
        'Authorization'=$accessToken
    }    

    Stop-AzWebApp -ResourceGroupName $azure_resourcegroup -Name $azure_webapp_name -Verbose  
    Start-Sleep 5
    
    $result = Invoke-RestMethod -Uri $apiUrl -Headers $Header -Method put -InFile $package_file -ContentType 'application/zip'
    write-host "result = " + $result -verbose
    #New-AzureWebsiteJob -Name $azure_webapp_name -JobName $azure_webjob_name -JobType "continuous" -JobFile $package_file -Verbose -ErrorAction Stop | Out-Null
    Start-AzWebApp -ResourceGroupName $azure_resourcegroup -Name $azure_webapp_name -Verbose

    Write-Host "Published webjob $azure_webjob_name" -foregroundColor Cyan
}
catch {
    Write-Host "$_.Exception.Message" -foregroundColor Red       
    throw           
}





