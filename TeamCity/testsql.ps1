﻿
function Ensure-HgXml {
    param([string]$raw_xml)

    $index = $raw_xml.IndexOf('<log')
    $content = $raw_xml.Substring($index)

    $content = $content.trim()
    
    [string]$rightSingleQuote = [System.Text.Encoding]::Default.GetString(146)
    if ($content.IndexOf($rightSingleQuote) -ge 0) {
        Write-Log -message 'Corrected right single quotes.'
        $content = $content.Replace($rightSingleQuote, "'")
    }
    return [xml]$content
}

$hg = 'hg'
$revBaseStart = '59d7de5116d0'
$branch_name = & hg branch

$revision = hg log -R \\mercurial_vm\repositories\encompass_kanban -r $revBaseStart --style=\\mercurial_vm\repositories\svnxml.tmpl
[xml]$xml_revision = Ensure-HgXml $revision
$rev_date = ($xml_revision.log.logentry.date).split('T')[0]

$hgArgs = @('log', '-R', '\\mercurial_vm\repositories\encompass_kanban', '-b', $branch_name, '-d', "$rev_date to today", '--style=\\mercurial_vm\repositories\svnxml.tmpl')
$xml = (& $hg $hgArgs) -join ''


$xml | out-file 'C:\DevOps\logs\sorting\sorted.txt'

#$reverse = hg reverse($xml)

#write-host $xml


#get date modified from each file in $sqlDirectory


hg log --template '{date|rfc822date}' -l 1 'C:\mercurial\encompass_kanban\sql\site\Tables\EventCheckIn_CREATE.sql'
