﻿

#$agents = invoke-RestMethod -Uri 'http://buildserver.local.imodules.com:90/guestAuth/app/rest/agents' -Method GET
$agents = invoke-RestMethod -Uri 'http://teamcitysand.local.imodules.com:90/guestAuth/app/rest/agents' -Method GET

write-host "==============BUILDSERVERS==============="
$agentnames = $agents.agents.agent

foreach($agent in $agentnames){
    $name = $agent.name
    write-host $name

    Invoke-Command -ComputerName $name -ScriptBlock { find-module -ListAvailable  }

}