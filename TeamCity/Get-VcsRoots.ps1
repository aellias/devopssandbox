﻿Function Get-TeamCity-Vcs-Roots([string]$baseUrl = "http://buildserver.local.imodules.com:90", [string]$projectId) {

    $url = "${baseUrl}guestAuth/app/rest/vcs-roots"

    $xml = [xml](invoke-RestMethod -Uri $url -Method GET)
    $xpath = "/builds/build[1]"
    $latestBuild = Select-xml -xpath $xpath -xml $xml

    return @{
        "Build" = $latestBuild.Node.GetAttribute("id");
        "Revision" = $latestBuild.Node.GetAttribute("number");
        "Url" = $url
        }
}