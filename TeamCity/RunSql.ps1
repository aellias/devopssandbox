$connection = New-Object System.Data.SqlClient.SqlConnection
$connection.ConnectionString = "Server=.\SQLExpress;Database=OctoFX;Integrated Security=True;"
$connection.ConnectionString = "Server=qasqlmaster;Database=imod_master;User Id=#{DBUserName};Password=#{DBPassword};"
Register-ObjectEvent -inputobject $connection -eventname InfoMessage -action {
    write-host $event.SourceEventArgs
} | Out-Null

function Execute-SqlQuery($query) {
    $queries = [System.Text.RegularExpressions.Regex]::Split($query, "^\s*GO\s*`$", [System.Text.RegularExpressions.RegexOptions]::IgnoreCase -bor [System.Text.RegularExpressions.RegexOptions]::Multiline)

    $queries | ForEach-Object {
        $q = $_
        if ((-not [String]::IsNullOrWhiteSpace($q)) -and ($q.Trim().ToLowerInvariant() -ne "go")) {            
            $command = $connection.CreateCommand()
            $command.CommandText = $q
            $command.CommandTimeout = $OctopusParameters['CommandTimeout']
            $command.ExecuteNonQuery() | Out-Null
        }
    }
}

Write-Host "Connecting"
try {
    $connection.Open()
    Write-Host "Executing script in qasqlmaster"
    $content = [IO.File]::ReadAllText("IModMaster_ROLL.sql")
    Execute-SqlQuery -query $content
}
catch {	
    throw
}
finally {
    Write-Host "Closing connection"
    $connection.Dispose()
}