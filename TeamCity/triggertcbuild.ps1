﻿
$teamcityHost = "https://teamcitysand.local.imodules.com"

function getAuthHeader {
    $user = 'devops'
    $pass = 'Jecn38ahafneqa83hruyw23dhkjsUje63hnvb2asg'
    $pair = "$($user):$($pass)"
    $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
    $base64 = [System.Convert]::ToBase64String($bytes)
    $basicAuthValue = "Basic $base64"

    return $basicAuthValue
}

function queueBuild {
    param ($channel, $branch)
    
    #create parameter string
    $params = '<property name="system.QaChannel" value="' + $channel +'"/><property name="system.DeployRelease" value="yes"/>'

    #create request object
    [xml]$request = $('
        <build branchName="' + $branch + '">
            <buildType id="EncompassMain_Package"/>
            <triggeringOptions queueAtTop="true"/>        
            <properties>' + $params + '</properties>
        </build>
    ')        

    #invoke request
    [xml]$response = Invoke-WebRequest -UseBasicParsing -Method Post -Uri $($teamcityHost + "/httpAuth/app/rest/buildQueue") -Body $request -Headers @{ Authorization = $(getAuthHeader) } -ContentType "application/xml"
    

    # Failed to queue build
    if ($response.build.state -ne "queued") {
      Write-Host "Failed to queue build"
      exit 1;
    } else {
        Write-Host $("Build started with ID " + $response.build.id)
    }

}

#get all active branches
set-location C:\mercurial\encompass_kanban
$branches = hg branches --active

foreach ($branch in $branches) {
    #get branch name
    $branch_name = $branch.split(' ')[0]
    
    
    #get release branches
    if ($branch_name -match "release") {

        $parts = $branch_name.Split(".")
        $versionDigit = $parts[1]
        Write-log "branch name = $branch_name versionDigit = $versionDigit"

        $result = $versionDigit % 2
         
        if ($Result -eq 0) {
            $QaChannel = "even"
            queueBuild -channel $QaChannel -branch $branch_name
        }
 
        if ($Result -eq 1) {
            $QaChannel = "odd"
            queueBuild -channel $QaChannel -branch $branch_name
        }
    }    
    
    #get hotfix branch    
    if ($branch_name -match "hotfix") {
        $QaChannel = "hotfix"
        queueBuild -channel $QaChannel -branch $branch_name
    }

}





#$json = "{'branchName':"$branch_name",'buildType':{'id':'EncompassMain_Package','projectId':'EncompassMain'},'properties':{'property':[{'name':'QaChannel','value':"$QaChannel"}]}}"
#write-host '<properties>'$params'</properties>' -ForegroundColor Green