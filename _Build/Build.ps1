import-module Encompass.Devops

$properties = "$PSScriptRoot\Properties.Common.ps1"
.$properties

FormatTaskName "$([Environment]::NewLine)==================== $(Get-Date -format T) - Executing {0} ====================" 

task publish -depends PrintInformation, PackageModules, PublishModulesToProget, PublishScriptsToProget, PublishDscResourcesToAzure

task PrintInformation { 
    $timestamp = "[{0:MM/dd/yy} {0:HH:mm:ss}]" -f (Get-Date)
    Remove-Item $global:logSettings.logFilePath -Recurse -Force -ErrorAction SilentlyContinue
    Write-Log '##########################################'
    Write-Log 'Build started -- build called'
    Write-Log $timestamp
    Write-Log $([Environment]::NewLine)
    Write-Log "In TeamCity: $inTeamCity"
    Write-Log "Teamcity build number: $env:build_number"
    Write-Log "Teamcity checkout directory: $env:checkout_directory"
    Write-Log $([Environment]::NewLine)
    Write-Log "build_working_dir: $build_working_dir"
    Write-Log '##########################################'
}

task PackageModules -depends IncrementModuleVersions, UpdatePsm1FileList, UpdatePsd1ExportedFunctionList {}

task IncrementModuleVersions {

    foreach ($module in $modules_list) {    

        $module_dir = "$build_working_dir\$module" 
        $module_manifest_file = "$module_dir\$module.psd1"

        Update-MetaData -Path $module_manifest_file -Increment 'Revision'
        Write-Host 'updated version' -ForegroundColor Green
    }    
    
}

task UpdatePsm1FileList {

    $modules_list | Set-ModuleFileListInPsm  
}


task UpdatePsd1ExportedFunctionList {

    $modules_list | Set-ModuleFunctionsToExport  
}

task PublishModulesToProget {    
    
    foreach ($module in $modules_list) {       

        $module_dir = "$build_working_dir\$module" 
        $module_manifest_file = "$module_dir\$module.psd1"

        $localModule = Get-Module -ListAvailable -Name "$module_dir"
        Write-Host "Local $($localModule.Name) has version $($localModule.Version )"

        $galleryModule = Find-Module -Name "$module" -Repository 'InternalPowerShellModules' -ErrorAction Stop
        Write-Host "Gallery $($galleryModule.Name) has version $($galleryModule.Version )"

        if ($localModule.Version -gt $galleryModule.Version) {
            Write-Host "Publishing module: $module to proget" -ForegroundColor Green
            Publish-Module -Path $module_dir -Repository "InternalPowerShellModules" -NuGetApiKey "1PAgtQMrXNEq7Z35_0xe" -Verbose        
        }

    }

}

task PublishDscModuleToProget {    
    
    $module_name = "Encompass.Dsc"
    $module_dir = "$build_working_dir\$module_name"
    $module_manifest_file = "$module_dir\$module.psd1"

    Write-Host "Publishing module: $module to proget" -ForegroundColor Green
    Publish-Module -Path $module_dir -Repository "InternalPowerShellModules" -NuGetApiKey "1PAgtQMrXNEq7Z35_0xe" -Verbose
}

task PublishScriptsToProget {
    
    Publish-Script -Path "$build_working_dir\defaultProfile.ps1" -Repository "InternalPowerShellModules" -NuGetApiKey "1PAgtQMrXNEq7Z35_0xe" -Verbose   
}

task PublishDscResourcesToAzure -depends PublishModulesToProget {

    $module_name = "Encompass.Dsc"
    $module_dir = "$build_working_dir\$module_name"
    $module_manifest_file = "$module_dir\$module.psd1"

    $module = get-module -name "$module_dir\$module_name.psd1" -ListAvailable
    $version = $module.Version  

    $module_zip = "$build_working_dir\$module_name.zip"    
    if (Test-Path -Path module_zip){
        Remove-Item -Path $module_zip
    }
    7zip a -mx=1 $module_zip "$module_dir\*"

    $SecurePassWord = ConvertTo-SecureString -AsPlainText 'Jecn38ah#afneqa83hruyw23@dhkjsUje63hnvb2asf' -Force
    $devappsCred = New-Object System.Management.Automation.PSCredential ("devapps", $SecurePassWord) 
   
    $azure_automation_account_name = "DevOps-Automation"
    $azure_resource_group_name = "DevOpsAuto"

    try {
        $credential = Get-DevAppsAzureCredential
        
        Disconnect-AzureRmAccount -ErrorAction SilentlyContinue
        Connect-AzureRmAccount -Credential $credential
        
        Get-AzureRmStorageAccount –Name devopsmodulestorage | Set-AzureStorageBlobContent –Container "modules" -File $module_zip -Blob "encompass.dsc" -Force
        $uri = $blob.ICloudBlob.Uri.AbsoluteUri    

        write-host "New-AzureRmAutomationModule Uri: $uri Zip: $module_zip Version: $version" -ForegroundColor green

        $ImportJob = New-AzureRmAutomationModule -AutomationAccountName $azure_automation_account_name -Name $module_name -ContentLinkUri "$uri/$module_name.zip" -ResourceGroupName $azure_resource_group_name -Verbose 
        #$ImportJob = Set-AzureRmAutomationModule -AutomationAccountName $azure_automation_account_name -Name "$module_name" -ContentLinkUri $uri -ResourceGroupName $azure_resource_group_name -ContentLinkVersion $version -Verbose
   
        Write-Output -InputObject 'Extracting module activities. Please wait.'
        $bImportCompleted = $false
        Do {
            Write-Output "Sleep for 5 sec..."
            Start-Sleep -Seconds 5
            $dscModule = Get-AzureRmAutomationModule -Name $module_name -ResourceGroupName $azure_resource_group_name -AutomationAccountName $azure_automation_account_name
            If ($dscModule.ProvisioningState -eq 'Succeeded') {
                Write-Output -InputObject 'Module import completed.'
                $bImportCompleted = $true
            }
            elseif ($dscModule.ProvisioningState -eq 'Failed') {
                Write-Error -Message 'Module import failed. Please manually check the error details in the Azure Portal.'
                $bImportCompleted = $true
            }
        }
        Until ($bImportCompleted -eq $true)

        #$existingDscModule = Get-AzureRmAutomationModule -Name "$module_name" -ResourceGroupName $azure_resource_group_name -AutomationAccountName $azure_automation_account_name 
        #Set-AzureRmAutomationModule -AutomationAccountName $azure_automation_account_name -Name "$module_name" -ContentLinkUri $uri -ResourceGroupName $azure_resource_group_name -Verbose -ContentLinkVersion "1.1.0.1" 
    }
    catch [System.Exception] {
        Write-Log -message $_.Exception.ToString()
    }
    
    
}

function Set-ModuleFunctionsToExport {
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [string[]]$module_name
    )
    
    BEGIN {}
    PROCESS {

        foreach ($name in $module_name) {

            $module_dir = "$build_working_dir\$name"         
            $module_manifest_file = "$module_dir\$name.psd1" 

            if (Test-Path "$module_dir\public") {

                $publicFunctions = new-object System.Collections.ArrayList    
                $files = Get-ChildItem -Path "$module_dir\public" -Recurse -file -include *.ps1
                Write-Host "$module_dir\public" -ForegroundColor Green

                foreach ($fileName in $files) {
                    $file = Split-Path $fileName -leaf
                    $function_name =  $file.Replace('.ps1', '')

                    Write-Host "function_name: $function_name" -ForegroundColor Green
                    [void]$publicFunctions.Add($function_name)
                }            

                Write-Host $publicFunctions -ForegroundColor Green
                Set-ModuleFunctions -Name $module_manifest_file -FunctionsToExport $publicFunctions
            }
        }

    }
    END {}
}

function Set-ModuleFileListInPsm {
    param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [string[]]$module_name
    )
    
    BEGIN {}
    PROCESS {

        foreach ($name in $module_name) {

            $psm1_content = [System.Text.StringBuilder]::new()
            $module_dir = "$build_working_dir\$name" 

            $module_file_list = Get-ChildItem -Path $module_dir -Recurse -file -include *.ps1

            foreach ($file in $module_file_list) {
                $file_to_add = $file.FullName.Replace("$module_dir\", "")
                [void]$psm1_content.Append(". `$PSScriptRoot\$file_to_add`n")
            }
            Set-Content -Path "$module_dir\$name.psm1" -Value $psm1_content
        }

    }
    END {}
}

function Set-SymlinksForModuleDir {
    param (
        $module_name
    )
    
    $module_dir = "$build_working_dir\$module_name" 
    New-SymLink -Path $module_dir -SymName "C:\Program Files\WindowsPowerShell\Modules\$($module_name)" -Directory
}

TaskSetup {
    
    $blockName = $($psake.context.Peek().currentTaskName)
    if ($env:TEAMCITY_VERSION) {
        Write-Output "##teamcity[blockOpened name='$blockName']"
    }
}

TaskTearDown {
    #TeamCity-ReportBuildFinish "Finishing task $($psake.context.Peek().currentTaskName)"

    $blockName = $($psake.context.Peek().currentTaskName)
    if ($env:TEAMCITY_VERSION) {
        Write-Output "##teamcity[blockClosed name='$blockName']"
    }
}