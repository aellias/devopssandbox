Write-Host "Start import properties"
properties {  
    
    $inTeamCity = (![String]::IsNullOrEmpty($env:TEAMCITY_VERSION))     
              
    $build_server_dir = 'C:\BuildServer'
    $7zip_path = "$build_server_dir\BuildTools\7-Zip\7z.exe"
    Set-Alias 7zip $7zip_path
   
    $global:logSettings = @{
        logToFile   = $true;
        logFilePath = "$PSScriptRoot\log-$logFileNameSegment.txt";
    }
    
    if ($inTeamCity -eq $false ) {        
        $now = Get-Date
        $logFileNameSegment = $now.ToString("yyyy_MM_dd_HH_mm_ss")
        $global:logSettings.logFilePath = "C:\Logs\DevOps\Encompass.OctopusDeploy\editor-api\log-$logFileNameSegment.txt";
    }

    $logFile_dir = [System.IO.Path]::GetDirectoryName($global:logSettings.logFilePath)
    if ((Test-Path $logFile_dir) -eq $False) {
        CreateDirectory -directoryName $logFile_dir      
    }

    $modules_list = @("Encompass.Build", "Encompass.Devops", "Encompass.OctopusDeploy", "Encompass.Dsc")
    $scripts_list = @("defaultProfile.ps1")
}
Write-Host "End import properties"


