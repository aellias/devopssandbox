param(
    [parameter(Mandatory = $true)] [string]$task_filename,     
    [parameter(Mandatory = $true)] [string]$task_name
)

if ($env:TEAMCITY_VERSION) {
    $host.UI.RawUI.BufferSize = New-Object System.Management.Automation.Host.Size(8192, 50)
}

function Get-TeamCityProperties {
    
    [OutputType([hashtable])]
    param(
        [string]$propertiesfile = $env:TEAMCITY_BUILD_PROPERTIES_FILE + '.xml'
    )

    if (![String]::IsNullOrEmpty($env:TEAMCITY_VERSION)) {
        Write-Verbose -Message "Loading TeamCity properties from $propertiesfile"
        $propertiesfile = (Resolve-Path $propertiesfile).Path

        $buildPropertiesXml = New-Object -TypeName System.Xml.XmlDocument
        $buildPropertiesXml.XmlResolver = $null
        $buildPropertiesXml.Load($propertiesfile)

        $buildProperties = @{}
        foreach ($entry in $buildPropertiesXml.SelectNodes('//entry')) {
            $buildProperties[$entry.Key] = $entry.'#text'
        }
        
        Write-Output -InputObject $buildProperties
    }
}


Import-Module Psake

$build_working_dir = (get-item $PSScriptRoot ).Parent.FullName
$branch_name = ""
$teamcity_build_number = ""  

Write-Host "build_working_dir = $build_working_dir" -ForegroundColor "Green"
Set-Location $build_working_dir

$branch_name = (& git rev-parse --abbrev-ref HEAD)   

Write-Host '##########################################'

if ($env:TEAMCITY_VERSION) {   
       
    $tcProperties = Get-TeamCityProperties
    $build_working_dir = $tcProperties["teamcity.build.workingDir"]
    $teamcity_build_number = $tcProperties['build.number']
    $teamcity_agent_name = $tcProperties['agent.name']

    Write-Host "TEAMCITY_BUILD_PROPERTIES_FILE = $env:TEAMCITY_BUILD_PROPERTIES_FILE" -ForegroundColor "Green"
    Write-Host "TEAMCITY_VERSION = $env:TEAMCITY_VERSION" -ForegroundColor "Green"
    Write-Host "teamcity_agent_name = $teamcity_agent_name" -ForegroundColor "Green"
    Write-Host "checkout_directory = $env:checkout_directory" -ForegroundColor "Green"
    Write-Host "teamcity_build_number = $teamcity_build_number" -ForegroundColor "Green"
}
else {  
}

Write-Host "build_working_dir = $build_working_dir" -ForegroundColor "Green"
Write-Host "branch_name = $branch_name" -ForegroundColor "Green"
Write-Host '##########################################'

$psakeArgs = @{
                build_working_dir = $build_working_dir; 
                branch_name = $branch_name; 
                teamcity_build_number = $teamcity_build_number;
            }


$psake.use_exit_on_error = $true
invoke-psake -buildfile $PSScriptRoot\$task_filename.ps1 -task $task_name -parameters $psakeArgs


